<aside class="sidebar hidden bg-gray-800 w-64 fixed left-0 inset-y-0 z-40">
    <a href="/app" class="flex items-center h-16 px-4 bg-gray-900">
        <img class="h-8 w-8" src="/img/logos/workflow-mark-on-dark.svg" alt="">

        <span class="text-xl text-white font-semibold pl-4">
            Recruitment
        </span>
    </a>

    <div class="sidebar-menu">
        <div class="px-3 pb-6">
            <a href="/app/recruitment" class="sidebar-menu__item {{ isActive(null, 3) }}">
                Dashboard
            </a>
        </div>

        <div class="py-6 border-t border-gray-700">
            <h4 class="px-6 text-sm text-gray-600 uppercase font-bold tracking-widest">
                Resources
            </h4>

            @can('category:view')
                <div class="px-3 mt-2">
                    <route-link href="recruitment/categories">Categories</route-link>
                </div>
            @endcan

            @can('type:view')
                <div class="px-3 mt-2">
                    <route-link href="recruitment/types">Types</route-link>
                </div>
            @endcan

            @can('location:view')
                <div class="px-3 mt-2">
                    <route-link href="recruitment/locations">Locations</route-link>
                </div>
            @endcan

            @can('process:view')
                <div class="px-3 mt-2">
                    <route-link href="recruitment/processes">Hiring Process</route-link>
                </div>
            @endcan

            @can('job:view')
                <div class="px-3 mt-2">
                    <route-link href="recruitment/jobs">Jobs</route-link>
                </div>
            @endcan

            @can('applicant:view')
                <div class="px-3 mt-2">
                    <route-link href="recruitment/applicants">Applicants</route-link>
                </div>
            @endcan
        </div>

        <div class="py-6 border-t border-gray-700">
            <h4 class="px-6 text-sm text-gray-600 uppercase font-bold tracking-widest">
                Others
            </h4>

            @can('user:view')
                <div class="px-3 mt-2">
                    <route-link href="users">Users</route-link>
                </div>
            @endcan

            @can('authorization:view')
                <div class="px-3 mt-2">
                    <route-link href="authorization/roles">Roles & Permissions</route-link>
                </div>
            @endcan

            @can('setting')
                <div class="px-3 mt-2">
                    <route-link href="settings/general">Settings</route-link>
                </div>
            @endcan

            @can('setting')
                <div class="px-3 mt-2">
                    <route-link href="languages/default">Languages</route-link>
                </div>
            @endcan
        </div>
    </div>
</aside>
