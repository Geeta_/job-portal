@component('mail::message')
# {{ $applicant->name }}

New applicant applied on job.

@component('mail::button', ['url' => config('app.url').'/admin/applicants/'.$applicant->id])
Click to See
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
