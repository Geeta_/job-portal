<?php

use Illuminate\Database\Seeder;

class ApplicantsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('applicants')->delete();

        \DB::table('applicants')->insert(array (
            0 =>
            array (
                'id' => 1,
                'job_id' => 1,
                'process_id' => 20,
                'name' => 'Applicant One',
                'email' => 'applicant_one@xyz.com',
                'phone' => '03007418529',
                'resume' => 'resumes/baUZ9sEUB3UFMKBfGuWlK3Q7kczsJNpOBkW5oOdx.pdf',
                'cover_letter' => 'Joseph Q. Applicant
123 Main Street
Anytown, CA 12345
555-212-1234
josephq@email.com

September 1, 2018

Jane Smith
Director, Human Resources
Fit Living
123 Business Rd.
Business City, NY 54321

Dear Ms. Smith:

I’m writing to apply for the position of Social Media Manager for Fit Living, as advertised on your website careers page. I have three years of experience as a Social Media Assistant for Young Living, and I believe I am ready to move up to the manager position.

In your job posting, you mention that you want to hire a Social Media Manager who understands Internet and social media trends. During my time at Young Living, I was given the responsibility of increasing follower numbers on Instagram. I explained to my manager that I would be happy to do so, and that I would also work hard to increase follower participation, because engagement has become an important metric.

Within six months, I increased our followers by over 50 percent and increased engagement by 400 percent. I’m very proud of that accomplishment. Currently, I’m working to build a following with the best influencers in our niche.

When I saw the job opening, I knew it was the perfect opportunity to offer you both my social media marketing skills and people skills. I’ve included my resume so you can learn more about my educational background and all of my work experience.Thank you for your time and consideration.

Please feel free to email me or call my cell phone at 555-555-5555. I hope to hear from you soon.

Sincerely,

Your Signature (hard copy letter)

Joseph Q. Applicant',
                'status' => 'Applied',
                'created_at' => '2019-07-30 11:23:14',
                'updated_at' => '2019-11-18 14:04:35',
            ),
            1 =>
            array (
                'id' => 2,
                'job_id' => 5,
                'process_id' => 16,
                'name' => 'Jane Smith',
                'email' => 'jane_smith@xyz.com',
                'phone' => '03001234567',
                'resume' => 'resumes/Y7xrVQtzu22nOQHPhlEFDjhxLtZ9J135O3CfHIzs.pdf',
                'cover_letter' => 'Joseph Q. Applicant 123 Main Street Anytown, CA 12345 555-212-1234 josephq@email.com

September 1, 2018

Jane Smith Director, Human Resources Fit Living 123 Business Rd. Business City, NY 54321

Dear Ms. Smith:

I’m writing to apply for the position of Social Media Manager for Fit Living, as advertised on your website careers page. I have three years of experience as a Social Media Assistant for Young Living, and I believe I am ready to move up to the manager position.

In your job posting, you mention that you want to hire a Social Media Manager who understands Internet and social media trends. During my time at Young Living, I was given the responsibility of increasing follower numbers on Instagram. I explained to my manager that I would be happy to do so, and that I would also work hard to increase follower participation, because engagement has become an important metric.

Within six months, I increased our followers by over 50 percent and increased engagement by 400 percent. I’m very proud of that accomplishment. Currently, I’m working to build a following with the best influencers in our niche.

When I saw the job opening, I knew it was the perfect opportunity to offer you both my social media marketing skills and people skills. I’ve included my resume so you can learn more about my educational background and all of my work experience.Thank you for your time and consideration.

Please feel free to email me or call my cell phone at 555-555-5555. I hope to hear from you soon.

Sincerely,

Your Signature (hard copy letter)

Joseph Q. Applicant',
                'status' => 'Applied',
                'created_at' => '2019-07-30 11:26:00',
                'updated_at' => '2019-07-30 11:26:00',
            ),
            2 =>
            array (
                'id' => 3,
                'job_id' => 4,
                'process_id' => 21,
                'name' => 'Molly Smith',
                'email' => 'molly_smith@xyz.com',
                'phone' => '03017654321',
                'resume' => 'resumes/p6i9ZhFLi3MO6B6SxUK3qtsaCcCkVO78hrbaiYaY.pdf',
                'cover_letter' => 'Molly Smith
21 Spring Street
Anycity, NY 12000
555-122-3333
msmith@email.com

August 1, 2018

John Brown
Sales Manager
Acme Corp.
321 Main Street
Anycity, NY 12000

Dear Mr. Brown,

I wish to apply for the sales position advertised on Monster.com. Terry Johnson suggested that I contact you directly, as we have worked together, and he felt that I would be a good fit with your team.

For the past two years I have been working in sales for Goodman & Co.. I have consistently exceeded my targets and I was recognized last quarter for outstanding service. As an avid cyclist and user of many of your products, I\'m aware that Acme Corp. is a company with tremendous potential. I am confident that my experience, communication skills, and ability to convey product benefits effectively would enable me to excel in the sales role.

I would be delighted to discuss with you how I might be an asset to the Acme Corp. sales team. Thank you for your consideration; I look forward to hearing from you.

Respectfully yours,

Molly Smith',
                'status' => 'Applied',
                'created_at' => '2019-07-30 11:46:39',
                'updated_at' => '2019-11-18 14:04:38',
            ),
            3 =>
            array (
                'id' => 4,
                'job_id' => 2,
                'process_id' => 20,
                'name' => 'John Donaldson',
                'email' => 'john_donaldson@xyz.com',
                'phone' => '03027654321',
                'resume' => 'resumes/RwQjgm6kyQAe47277RvrdlX475GCpQYA9BlRsTVJ.pdf',
                'cover_letter' => 'John Donaldson
8 Sue Circle
Smithtown, CA 08067
909-555-5555
john.donaldson@emailexample.com

September 1, 2018

George Gilhooley
Times Union
87 Delaware Road
Hatfield, CA 08065

Dear Mr. Gilhooley,

I am writing to apply for the programmer position advertised in the Times Union. As requested, I enclose a completed job application, my certification, my resume and three references.

The role is very appealing to me, and I believe that my strong technical experience and education make me a highly competitive candidate for this position. My key strengths that would support my success in this position include:

I have successfully designed, developed and supported live-use applications.
I strive continually for excellence.
I provide exceptional contributions to customer service for all customers.
With a BS degree in Computer Programming, I have a comprehensive understanding of the full lifecycle for software development projects. I also have experience in learning and applying new technologies as appropriate. Please see my resume for additional information on my experience.

I can be reached anytime via email at john.donaldson@emailexample.com or by cell phone, 909-555-5555.

Thank you for your time and consideration. I look forward to speaking with you about this employment opportunity.

Sincerely,

Signature (hard copy letter)

John Donaldson',
                'status' => 'Applied',
                'created_at' => '2019-07-30 11:47:36',
                'updated_at' => '2019-11-18 14:12:02',
            ),
            4 =>
            array (
                'id' => 5,
                'job_id' => 3,
                'process_id' => 16,
                'name' => 'Colleen Warren',
                'email' => 'colleen.warren@noemail.com',
                'phone' => '03037654321',
                'resume' => 'resumes/VOlbFNPz5VWYolSvYFDRlyoOAYOQp9ealg7H6iWy.pdf',
                'cover_letter' => 'Dear Hiring Manager,

I\'m writing to express my interest in the Web Content Manager position listed on Monster.com. I have experience building large, consumer-focused health-based content sites. While much of my experience has been in the business world, I understand the social value of this sector and I am confident that my business experience will be an asset to your organization.

My responsibilities have included the development and management of: website editorial voice and style; editorial calendars; and the daily content programming and production for various websites.

I have worked closely with healthcare professionals and medical editors to help them to provide the best possible information to a consumer audience of patients. I have also helped physicians to use their medical content to write user-friendly and easily comprehensible text.

Experience has taught me how to build strong relationships with all departments in an organization. I have the ability to work within a team as well as cross-team. I can work with web engineers to resolve technical issues and implement technical enhancements.

I am confident working with development departments to implement design and functional enhancements, and to monitor site statistics and conduct search engine optimization.

Thank you for your consideration.

Colleen Warren
colleen.warren@noemail.com
555-123-1234
www.linked.com/colleenwarren',
                'status' => 'Applied',
                'created_at' => '2019-07-30 11:49:15',
                'updated_at' => '2019-07-30 11:49:15',
            ),
            5 =>
            array (
                'id' => 6,
                'job_id' => 5,
                'process_id' => 16,
                'name' => 'James I. Smith',
                'email' => 'james_i_smith@xyz.com',
                'phone' => '03047654321',
                'resume' => 'resumes/3Qb0qPpZ561kgerf7rE0czl9rh2R7qS7othLQRdS.pdf',
                'cover_letter' => 'Molly Smith
21 Spring Street
Anycity, NY 12000
555-122-3333
msmith@email.com

August 1, 2018

John Brown
Sales Manager
Acme Corp.
321 Main Street
Anycity, NY 12000

Dear Mr. Brown,

I wish to apply for the sales position advertised on Monster.com. Terry Johnson suggested that I contact you directly, as we have worked together, and he felt that I would be a good fit with your team.

For the past two years I have been working in sales for Goodman & Co.. I have consistently exceeded my targets and I was recognized last quarter for outstanding service. As an avid cyclist and user of many of your products, I\'m aware that Acme Corp. is a company with tremendous potential. I am confident that my experience, communication skills, and ability to convey product benefits effectively would enable me to excel in the sales role.

I would be delighted to discuss with you how I might be an asset to the Acme Corp. sales team. Thank you for your consideration; I look forward to hearing from you.

Respectfully yours,

Molly Smith',
                'status' => 'Applied',
                'created_at' => '2019-07-30 11:55:32',
                'updated_at' => '2019-07-30 11:55:32',
            ),
            6 =>
            array (
                'id' => 7,
                'job_id' => 1,
                'process_id' => 16,
                'name' => 'Robert Cavanaugh',
                'email' => 'robert_cavanaugh@xyz.com',
                'phone' => '03067654321',
                'resume' => 'resumes/7rG0AHMqDjtCC3BOwdJCQmU1z3CMC3cIsAJ86OV7.pdf',
                'cover_letter' => 'Joseph Q. Applicant
123 Main Street
Anytown, CA 12345
555-212-1234
josephq@email.com

September 1, 2018

Jane Smith
Director, Human Resources
Fit Living
123 Business Rd.
Business City, NY 54321

Dear Ms. Smith:

I’m writing to apply for the position of Social Media Manager for Fit Living, as advertised on your website careers page. I have three years of experience as a Social Media Assistant for Young Living, and I believe I am ready to move up to the manager position.

In your job posting, you mention that you want to hire a Social Media Manager who understands Internet and social media trends. During my time at Young Living, I was given the responsibility of increasing follower numbers on Instagram. I explained to my manager that I would be happy to do so, and that I would also work hard to increase follower participation, because engagement has become an important metric.

Within six months, I increased our followers by over 50 percent and increased engagement by 400 percent. I’m very proud of that accomplishment. Currently, I’m working to build a following with the best influencers in our niche.

When I saw the job opening, I knew it was the perfect opportunity to offer you both my social media marketing skills and people skills. I’ve included my resume so you can learn more about my educational background and all of my work experience.Thank you for your time and consideration.

Please feel free to email me or call my cell phone at 555-555-5555. I hope to hear from you soon.

Sincerely,

Your Signature (hard copy letter)

Joseph Q. Applicant',
                'status' => 'Applied',
                'created_at' => '2019-07-30 11:57:11',
                'updated_at' => '2019-07-30 11:57:11',
            ),
            7 =>
            array (
                'id' => 8,
                'job_id' => 2,
                'process_id' => 16,
                'name' => 'Oscar Macdonald',
                'email' => 'oscar_macdonald@xyz.com',
                'phone' => '03077654321',
                'resume' => 'resumes/5wixMnpKXAalB4OcIKF7IGgQSvEDmJ6Sk7xWEbZc.pdf',
                'cover_letter' => 'Molly Smith
21 Spring Street
Anycity, NY 12000
555-122-3333
msmith@email.com

August 1, 2018

John Brown
Sales Manager
Acme Corp.
321 Main Street
Anycity, NY 12000

Dear Mr. Brown,

I wish to apply for the sales position advertised on Monster.com. Terry Johnson suggested that I contact you directly, as we have worked together, and he felt that I would be a good fit with your team.

For the past two years I have been working in sales for Goodman & Co.. I have consistently exceeded my targets and I was recognized last quarter for outstanding service. As an avid cyclist and user of many of your products, I\'m aware that Acme Corp. is a company with tremendous potential. I am confident that my experience, communication skills, and ability to convey product benefits effectively would enable me to excel in the sales role.

I would be delighted to discuss with you how I might be an asset to the Acme Corp. sales team. Thank you for your consideration; I look forward to hearing from you.

Respectfully yours,

Molly Smith',
                'status' => 'Applied',
                'created_at' => '2019-07-30 12:03:43',
                'updated_at' => '2019-07-30 12:03:43',
            ),
            8 =>
            array (
                'id' => 9,
                'job_id' => 5,
                'process_id' => 16,
                'name' => 'Deacon Mccray',
                'email' => 'deacon_mccray@xyz.com',
                'phone' => '03087654321',
                'resume' => 'resumes/zFkfOvJeC5fgGiD0pR5dIHc8Rs4IFGr39k4zX5je.pdf',
                'cover_letter' => 'Molly Smith
21 Spring Street
Anycity, NY 12000
555-122-3333
msmith@email.com

August 1, 2018

John Brown
Sales Manager
Acme Corp.
321 Main Street
Anycity, NY 12000

Dear Mr. Brown,

I wish to apply for the sales position advertised on Monster.com. Terry Johnson suggested that I contact you directly, as we have worked together, and he felt that I would be a good fit with your team.

For the past two years I have been working in sales for Goodman & Co.. I have consistently exceeded my targets and I was recognized last quarter for outstanding service. As an avid cyclist and user of many of your products, I\'m aware that Acme Corp. is a company with tremendous potential. I am confident that my experience, communication skills, and ability to convey product benefits effectively would enable me to excel in the sales role.

I would be delighted to discuss with you how I might be an asset to the Acme Corp. sales team. Thank you for your consideration; I look forward to hearing from you.

Respectfully yours,

Molly Smith',
                'status' => 'Applied',
                'created_at' => '2019-07-30 12:04:57',
                'updated_at' => '2019-07-30 12:04:57',
            ),
            9 =>
            array (
                'id' => 10,
                'job_id' => 1,
                'process_id' => 16,
                'name' => 'Mary K Strong',
                'email' => 'mary_strong@xyz.com',
                'phone' => '03097654321',
                'resume' => 'resumes/oloxEn5mRKcbzNvdkcCr7u5Zabi4Z8LB35C7NFZ8.pdf',
                'cover_letter' => 'Joseph Q. Applicant
123 Main Street
Anytown, CA 12345
555-212-1234
josephq@email.com

September 1, 2018

Jane Smith
Director, Human Resources
Fit Living
123 Business Rd.
Business City, NY 54321

Dear Ms. Smith:

I’m writing to apply for the position of Social Media Manager for Fit Living, as advertised on your website careers page. I have three years of experience as a Social Media Assistant for Young Living, and I believe I am ready to move up to the manager position.

In your job posting, you mention that you want to hire a Social Media Manager who understands Internet and social media trends. During my time at Young Living, I was given the responsibility of increasing follower numbers on Instagram. I explained to my manager that I would be happy to do so, and that I would also work hard to increase follower participation, because engagement has become an important metric.

Within six months, I increased our followers by over 50 percent and increased engagement by 400 percent. I’m very proud of that accomplishment. Currently, I’m working to build a following with the best influencers in our niche.

When I saw the job opening, I knew it was the perfect opportunity to offer you both my social media marketing skills and people skills. I’ve included my resume so you can learn more about my educational background and all of my work experience.Thank you for your time and consideration.

Please feel free to email me or call my cell phone at 555-555-5555. I hope to hear from you soon.

Sincerely,

Your Signature (hard copy letter)

Joseph Q. Applicant',
                'status' => 'Applied',
                'created_at' => '2019-07-30 12:12:28',
                'updated_at' => '2019-07-30 12:12:28',
            ),
            10 =>
            array (
                'id' => 11,
                'job_id' => 4,
                'process_id' => 16,
                'name' => 'Jacob K Rodriguez',
                'email' => 'jacob_rodriguez@xyz.com',
                'phone' => '03028654321',
                'resume' => 'resumes/VjRbUq9M5uZYDufD83cyFwNkaor6bQwEntXquc7J.pdf',
                'cover_letter' => 'Molly Smith
21 Spring Street
Anycity, NY 12000
555-122-3333
msmith@email.com

August 1, 2018

John Brown
Sales Manager
Acme Corp.
321 Main Street
Anycity, NY 12000

Dear Mr. Brown,

I wish to apply for the sales position advertised on Monster.com. Terry Johnson suggested that I contact you directly, as we have worked together, and he felt that I would be a good fit with your team.

For the past two years I have been working in sales for Goodman & Co.. I have consistently exceeded my targets and I was recognized last quarter for outstanding service. As an avid cyclist and user of many of your products, I\'m aware that Acme Corp. is a company with tremendous potential. I am confident that my experience, communication skills, and ability to convey product benefits effectively would enable me to excel in the sales role.

I would be delighted to discuss with you how I might be an asset to the Acme Corp. sales team. Thank you for your consideration; I look forward to hearing from you.

Respectfully yours,

Molly Smith',
                'status' => 'Applied',
                'created_at' => '2019-07-30 12:13:40',
                'updated_at' => '2019-07-30 12:13:40',
            ),
            11 =>
            array (
                'id' => 12,
                'job_id' => 3,
                'process_id' => 16,
                'name' => 'Mina R. Martinez',
                'email' => 'mina_martinez@xyz.com',
                'phone' => '03081478523',
                'resume' => 'resumes/xLnJOuLVysAWX9eYsOt1Rma4drVzD2m7cunNhiIy.pdf',
                'cover_letter' => 'Dear Hiring Manager,

I\'m writing to express my interest in the Web Content Manager position listed on Monster.com. I have experience building large, consumer-focused health-based content sites. While much of my experience has been in the business world, I understand the social value of this sector and I am confident that my business experience will be an asset to your organization.

My responsibilities have included the development and management of: website editorial voice and style; editorial calendars; and the daily content programming and production for various websites.

I have worked closely with healthcare professionals and medical editors to help them to provide the best possible information to a consumer audience of patients. I have also helped physicians to use their medical content to write user-friendly and easily comprehensible text.

Experience has taught me how to build strong relationships with all departments in an organization. I have the ability to work within a team as well as cross-team. I can work with web engineers to resolve technical issues and implement technical enhancements.

I am confident working with development departments to implement design and functional enhancements, and to monitor site statistics and conduct search engine optimization.

Thank you for your consideration.

Colleen Warren
colleen.warren@noemail.com
555-123-1234
www.linked.com/colleenwarren',
                'status' => 'Applied',
                'created_at' => '2019-07-30 12:14:55',
                'updated_at' => '2019-07-30 12:14:55',
            ),
            12 =>
            array (
                'id' => 13,
                'job_id' => 4,
                'process_id' => 16,
                'name' => 'Maria J. McAtee',
                'email' => 'maria_mcAtee@xyz.com',
                'phone' => '03019987456',
                'resume' => 'resumes/DIaObNVFUhOexR9NkMq1PPYP9j8knMQ65ek1wVNd.pdf',
                'cover_letter' => 'Molly Smith
21 Spring Street
Anycity, NY 12000
555-122-3333
msmith@email.com

August 1, 2018

John Brown
Sales Manager
Acme Corp.
321 Main Street
Anycity, NY 12000

Dear Mr. Brown,

I wish to apply for the sales position advertised on Monster.com. Terry Johnson suggested that I contact you directly, as we have worked together, and he felt that I would be a good fit with your team.

For the past two years I have been working in sales for Goodman & Co.. I have consistently exceeded my targets and I was recognized last quarter for outstanding service. As an avid cyclist and user of many of your products, I\'m aware that Acme Corp. is a company with tremendous potential. I am confident that my experience, communication skills, and ability to convey product benefits effectively would enable me to excel in the sales role.

I would be delighted to discuss with you how I might be an asset to the Acme Corp. sales team. Thank you for your consideration; I look forward to hearing from you.

Respectfully yours,

Molly Smith',
                'status' => 'Applied',
                'created_at' => '2019-07-30 12:21:03',
                'updated_at' => '2019-07-30 12:21:03',
            ),
            13 =>
            array (
                'id' => 14,
                'job_id' => 4,
                'process_id' => 16,
                'name' => 'Debra T. Mojica',
                'email' => 'debra_mojica@xyz.com',
                'phone' => '03008529637',
                'resume' => 'resumes/mP2Tni2Ea0kVFJ5M3dFHxA2lPm3yqOHsWlwvFUbh.pdf',
                'cover_letter' => 'Dear Hiring Manager,

I\'m writing to express my interest in the Web Content Manager position listed on Monster.com. I have experience building large, consumer-focused health-based content sites. While much of my experience has been in the business world, I understand the social value of this sector and I am confident that my business experience will be an asset to your organization.

My responsibilities have included the development and management of: website editorial voice and style; editorial calendars; and the daily content programming and production for various websites.

I have worked closely with healthcare professionals and medical editors to help them to provide the best possible information to a consumer audience of patients. I have also helped physicians to use their medical content to write user-friendly and easily comprehensible text.

Experience has taught me how to build strong relationships with all departments in an organization. I have the ability to work within a team as well as cross-team. I can work with web engineers to resolve technical issues and implement technical enhancements.

I am confident working with development departments to implement design and functional enhancements, and to monitor site statistics and conduct search engine optimization.

Thank you for your consideration.

Colleen Warren
colleen.warren@noemail.com
555-123-1234
www.linked.com/colleenwarren',
                'status' => 'Applied',
                'created_at' => '2019-07-30 12:22:39',
                'updated_at' => '2019-07-30 12:22:39',
            ),
            14 =>
            array (
                'id' => 15,
                'job_id' => 1,
                'process_id' => 16,
                'name' => 'Brian V. Dexter',
                'email' => 'brian_dexter@xyz.com',
                'phone' => '03023325698',
                'resume' => 'resumes/ioDEeoCZqAlmMe69JTgj77UVj0aiI8i1TumXrHBr.pdf',
                'cover_letter' => 'Joseph Q. Applicant
123 Main Street
Anytown, CA 12345
555-212-1234
josephq@email.com

September 1, 2018

Jane Smith
Director, Human Resources
Fit Living
123 Business Rd.
Business City, NY 54321

Dear Ms. Smith:

I’m writing to apply for the position of Social Media Manager for Fit Living, as advertised on your website careers page. I have three years of experience as a Social Media Assistant for Young Living, and I believe I am ready to move up to the manager position.

In your job posting, you mention that you want to hire a Social Media Manager who understands Internet and social media trends. During my time at Young Living, I was given the responsibility of increasing follower numbers on Instagram. I explained to my manager that I would be happy to do so, and that I would also work hard to increase follower participation, because engagement has become an important metric.

Within six months, I increased our followers by over 50 percent and increased engagement by 400 percent. I’m very proud of that accomplishment. Currently, I’m working to build a following with the best influencers in our niche.

When I saw the job opening, I knew it was the perfect opportunity to offer you both my social media marketing skills and people skills. I’ve included my resume so you can learn more about my educational background and all of my work experience.Thank you for your time and consideration.

Please feel free to email me or call my cell phone at 555-555-5555. I hope to hear from you soon.

Sincerely,

Your Signature (hard copy letter)

Joseph Q. Applicant',
                'status' => 'Applied',
                'created_at' => '2019-07-30 12:23:39',
                'updated_at' => '2019-07-30 12:23:39',
            ),
            15 =>
            array (
                'id' => 16,
                'job_id' => 4,
                'process_id' => 16,
                'name' => 'Richard T. Elwood',
                'email' => 'richard_elwood@xyz.com',
                'phone' => '03037456985',
                'resume' => 'resumes/8WwcxyRJfuBeTKuoFjJYJh65yWINu6RO4eyjzYuV.pdf',
                'cover_letter' => 'Molly Smith
21 Spring Street
Anycity, NY 12000
555-122-3333
msmith@email.com

August 1, 2018

John Brown
Sales Manager
Acme Corp.
321 Main Street
Anycity, NY 12000

Dear Mr. Brown,

I wish to apply for the sales position advertised on Monster.com. Terry Johnson suggested that I contact you directly, as we have worked together, and he felt that I would be a good fit with your team.

For the past two years I have been working in sales for Goodman & Co.. I have consistently exceeded my targets and I was recognized last quarter for outstanding service. As an avid cyclist and user of many of your products, I\'m aware that Acme Corp. is a company with tremendous potential. I am confident that my experience, communication skills, and ability to convey product benefits effectively would enable me to excel in the sales role.

I would be delighted to discuss with you how I might be an asset to the Acme Corp. sales team. Thank you for your consideration; I look forward to hearing from you.

Respectfully yours,

Molly Smith',
                'status' => 'Applied',
                'created_at' => '2019-07-30 12:24:57',
                'updated_at' => '2019-07-30 12:24:57',
            ),
            16 =>
            array (
                'id' => 17,
                'job_id' => 2,
                'process_id' => 16,
                'name' => 'Norbert H. Powell',
                'email' => 'norbert_powell@xyz.com',
                'phone' => '03004567891',
                'resume' => 'resumes/G35iYkG32ZO7WivH29fpmbItTyeCLuCMMAGVY0pk.pdf',
                'cover_letter' => 'Joseph Q. Applicant
123 Main Street
Anytown, CA 12345
555-212-1234
josephq@email.com

September 1, 2018

Jane Smith
Director, Human Resources
Fit Living
123 Business Rd.
Business City, NY 54321

Dear Ms. Smith:

I’m writing to apply for the position of Social Media Manager for Fit Living, as advertised on your website careers page. I have three years of experience as a Social Media Assistant for Young Living, and I believe I am ready to move up to the manager position.

In your job posting, you mention that you want to hire a Social Media Manager who understands Internet and social media trends. During my time at Young Living, I was given the responsibility of increasing follower numbers on Instagram. I explained to my manager that I would be happy to do so, and that I would also work hard to increase follower participation, because engagement has become an important metric.

Within six months, I increased our followers by over 50 percent and increased engagement by 400 percent. I’m very proud of that accomplishment. Currently, I’m working to build a following with the best influencers in our niche.

When I saw the job opening, I knew it was the perfect opportunity to offer you both my social media marketing skills and people skills. I’ve included my resume so you can learn more about my educational background and all of my work experience.Thank you for your time and consideration.

Please feel free to email me or call my cell phone at 555-555-5555. I hope to hear from you soon.

Sincerely,

Your Signature (hard copy letter)

Joseph Q. Applicant',
                'status' => 'Applied',
                'created_at' => '2019-07-30 12:25:39',
                'updated_at' => '2019-07-30 12:25:39',
            ),
            17 =>
            array (
                'id' => 18,
                'job_id' => 3,
                'process_id' => 16,
                'name' => 'Michael J. Becker',
                'email' => 'michael_becker@xyz.com',
                'phone' => '03051234569',
                'resume' => 'resumes/Joo310VUQKd0P9se4RBbPtaSDwKCUDUlLsWBFYhq.pdf',
                'cover_letter' => 'Molly Smith
21 Spring Street
Anycity, NY 12000
555-122-3333
msmith@email.com

August 1, 2018

John Brown
Sales Manager
Acme Corp.
321 Main Street
Anycity, NY 12000

Dear Mr. Brown,

I wish to apply for the sales position advertised on Monster.com. Terry Johnson suggested that I contact you directly, as we have worked together, and he felt that I would be a good fit with your team.

For the past two years I have been working in sales for Goodman & Co.. I have consistently exceeded my targets and I was recognized last quarter for outstanding service. As an avid cyclist and user of many of your products, I\'m aware that Acme Corp. is a company with tremendous potential. I am confident that my experience, communication skills, and ability to convey product benefits effectively would enable me to excel in the sales role.

I would be delighted to discuss with you how I might be an asset to the Acme Corp. sales team. Thank you for your consideration; I look forward to hearing from you.

Respectfully yours,

Molly Smith',
                'status' => 'Applied',
                'created_at' => '2019-07-30 12:26:43',
                'updated_at' => '2019-07-30 12:26:43',
            ),
            18 =>
            array (
                'id' => 19,
                'job_id' => 4,
                'process_id' => 16,
                'name' => 'Frank T. Kuo',
                'email' => 'frank_kuo@xyz.com',
                'phone' => '03018529633',
                'resume' => 'resumes/wh69tfv8CQOWH1ukxny0YoTAlwOrN2tNdWa0XWfC.pdf',
                'cover_letter' => 'Molly Smith
21 Spring Street
Anycity, NY 12000
555-122-3333
msmith@email.com

August 1, 2018

John Brown
Sales Manager
Acme Corp.
321 Main Street
Anycity, NY 12000

Dear Mr. Brown,

I wish to apply for the sales position advertised on Monster.com. Terry Johnson suggested that I contact you directly, as we have worked together, and he felt that I would be a good fit with your team.

For the past two years I have been working in sales for Goodman & Co.. I have consistently exceeded my targets and I was recognized last quarter for outstanding service. As an avid cyclist and user of many of your products, I\'m aware that Acme Corp. is a company with tremendous potential. I am confident that my experience, communication skills, and ability to convey product benefits effectively would enable me to excel in the sales role.

I would be delighted to discuss with you how I might be an asset to the Acme Corp. sales team. Thank you for your consideration; I look forward to hearing from you.

Respectfully yours,

Molly Smith',
                'status' => 'Applied',
                'created_at' => '2019-07-30 12:28:00',
                'updated_at' => '2019-07-30 12:28:00',
            ),
            19 =>
            array (
                'id' => 20,
                'job_id' => 2,
                'process_id' => 16,
                'name' => 'Marvin D. Kendall',
                'email' => 'marvin_kendall@xyz.com',
                'phone' => '03037418523',
                'resume' => 'resumes/KVBeYZFK3J6sVjWoryghfHa2YbgREDBBhRJjlDR2.pdf',
                'cover_letter' => 'Molly Smith
21 Spring Street
Anycity, NY 12000
555-122-3333
msmith@email.com

August 1, 2018

John Brown
Sales Manager
Acme Corp.
321 Main Street
Anycity, NY 12000

Dear Mr. Brown,

I wish to apply for the sales position advertised on Monster.com. Terry Johnson suggested that I contact you directly, as we have worked together, and he felt that I would be a good fit with your team.

For the past two years I have been working in sales for Goodman & Co.. I have consistently exceeded my targets and I was recognized last quarter for outstanding service. As an avid cyclist and user of many of your products, I\'m aware that Acme Corp. is a company with tremendous potential. I am confident that my experience, communication skills, and ability to convey product benefits effectively would enable me to excel in the sales role.

I would be delighted to discuss with you how I might be an asset to the Acme Corp. sales team. Thank you for your consideration; I look forward to hearing from you.

Respectfully yours,

Molly Smith',
                'status' => 'Applied',
                'created_at' => '2019-07-30 12:29:07',
                'updated_at' => '2019-07-30 12:29:07',
            ),
        ));


    }
}
