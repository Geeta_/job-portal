<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$psLg8Qm2wEVpHGwlMroFT.UHydhuHlpZaXoYRKB.56zt5h1RLLfeS',
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'User',
                'email' => 'user@user.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$2kCWITrbeK231zKbCfnsLughSLv8a2jMBPyDU0Ttx29A6Rvu66XPO',
                'remember_token' => NULL,
                'created_at' => '2019-11-16 08:03:05',
                'updated_at' => '2019-11-16 08:03:05',
            ),
        ));
        
        
    }
}