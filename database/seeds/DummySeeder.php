<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class DummySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $this->call(PermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(JobsTableSeeder::class);
        $this->call(OptionsTableSeeder::class);
        $this->call(ApplicantsTableSeeder::class);
        $this->call(LanguagesTableSeeder::class);

        $this->call(RoleUserTableSeeder::class);
        $this->call(PermissionRoleTableSeeder::class);

        Artisan::call('cache:clear');
    }
}
