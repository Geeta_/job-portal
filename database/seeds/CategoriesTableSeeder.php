<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'parent_id' => NULL,
                'order' => NULL,
                'name' => 'Job Category',
                'slug' => NULL,
                'key' => 'Job Category',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'parent_id' => NULL,
                'order' => NULL,
                'name' => 'Job Type',
                'slug' => NULL,
                'key' => 'Job Type',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'parent_id' => NULL,
                'order' => NULL,
                'name' => 'Job Location',
                'slug' => NULL,
                'key' => 'Job Location',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'parent_id' => NULL,
                'order' => NULL,
                'name' => 'Hiring Process',
                'slug' => NULL,
                'key' => 'Hiring Process',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'parent_id' => 1,
                'order' => NULL,
                'name' => 'Finance',
                'slug' => 'finance',
                'key' => NULL,
                'created_at' => '2019-11-16 05:28:33',
                'updated_at' => '2019-11-16 05:28:33',
            ),
            5 => 
            array (
                'id' => 6,
                'parent_id' => 1,
                'order' => NULL,
                'name' => 'Software Engineering',
                'slug' => 'software-engineering',
                'key' => NULL,
                'created_at' => '2019-11-16 05:28:44',
                'updated_at' => '2019-11-16 05:28:44',
            ),
            6 => 
            array (
                'id' => 7,
                'parent_id' => 1,
                'order' => NULL,
                'name' => 'Marketing',
                'slug' => 'marketing',
                'key' => NULL,
                'created_at' => '2019-11-16 05:28:53',
                'updated_at' => '2019-11-16 05:28:53',
            ),
            7 => 
            array (
                'id' => 8,
                'parent_id' => 2,
                'order' => NULL,
                'name' => 'Full Time',
                'slug' => 'full-time',
                'key' => NULL,
                'created_at' => '2019-11-16 05:35:33',
                'updated_at' => '2019-11-16 05:35:33',
            ),
            8 => 
            array (
                'id' => 9,
                'parent_id' => 2,
                'order' => NULL,
                'name' => 'Part Time',
                'slug' => 'part-time',
                'key' => NULL,
                'created_at' => '2019-11-16 05:35:41',
                'updated_at' => '2019-11-16 05:35:41',
            ),
            9 => 
            array (
                'id' => 10,
                'parent_id' => 2,
                'order' => NULL,
                'name' => 'Intern',
                'slug' => 'intern',
                'key' => NULL,
                'created_at' => '2019-11-16 05:35:47',
                'updated_at' => '2019-11-16 05:35:47',
            ),
            10 => 
            array (
                'id' => 11,
                'parent_id' => 2,
                'order' => NULL,
                'name' => 'Remote',
                'slug' => 'remote',
                'key' => NULL,
                'created_at' => '2019-11-16 05:35:54',
                'updated_at' => '2019-11-16 05:35:54',
            ),
            11 => 
            array (
                'id' => 12,
                'parent_id' => 3,
                'order' => NULL,
                'name' => 'San Francisco, CA, USA',
                'slug' => 'san-francisco-ca-usa',
                'key' => NULL,
                'created_at' => '2019-11-16 05:36:55',
                'updated_at' => '2019-11-16 05:36:55',
            ),
            12 => 
            array (
                'id' => 13,
                'parent_id' => 3,
                'order' => NULL,
                'name' => 'New York, USA',
                'slug' => 'new-york-usa',
                'key' => NULL,
                'created_at' => '2019-11-16 05:37:03',
                'updated_at' => '2019-11-16 05:37:03',
            ),
            13 => 
            array (
                'id' => 14,
                'parent_id' => 3,
                'order' => NULL,
                'name' => 'Atlanta, GA',
                'slug' => 'atlanta-ga',
                'key' => NULL,
                'created_at' => '2019-11-16 05:37:10',
                'updated_at' => '2019-11-16 05:37:10',
            ),
            14 => 
            array (
                'id' => 15,
                'parent_id' => 3,
                'order' => NULL,
                'name' => 'Mexico City',
                'slug' => 'mexico-city',
                'key' => NULL,
                'created_at' => '2019-11-16 05:37:19',
                'updated_at' => '2019-11-16 05:37:19',
            ),
            15 => 
            array (
                'id' => 16,
                'parent_id' => 4,
                'order' => NULL,
                'name' => 'Applied',
                'slug' => 'applied',
                'key' => 'Applied',
                'created_at' => '2019-11-16 05:38:18',
                'updated_at' => '2019-11-16 05:38:18',
            ),
            16 => 
            array (
                'id' => 17,
                'parent_id' => 4,
                'order' => NULL,
                'name' => 'Phone Screening',
                'slug' => 'phone-screening',
                'key' => NULL,
                'created_at' => '2019-11-16 05:37:44',
                'updated_at' => '2019-11-16 05:37:44',
            ),
            17 => 
            array (
                'id' => 18,
                'parent_id' => 4,
                'order' => NULL,
                'name' => 'Skype Interview',
                'slug' => 'skype-interview',
                'key' => NULL,
                'created_at' => '2019-11-16 05:37:52',
                'updated_at' => '2019-11-16 05:37:52',
            ),
            18 => 
            array (
                'id' => 19,
                'parent_id' => 4,
                'order' => NULL,
                'name' => 'Schedule Interview',
                'slug' => 'schedule-interview',
                'key' => NULL,
                'created_at' => '2019-11-16 05:38:00',
                'updated_at' => '2019-11-16 05:38:00',
            ),
            19 => 
            array (
                'id' => 20,
                'parent_id' => 4,
                'order' => NULL,
                'name' => 'Hired',
                'slug' => 'hired',
                'key' => 'Hired',
                'created_at' => '2019-11-16 05:38:12',
                'updated_at' => '2019-11-16 05:38:12',
            ),
            20 => 
            array (
                'id' => 21,
                'parent_id' => 4,
                'order' => NULL,
                'name' => 'Rejected',
                'slug' => 'rejected',
                'key' => 'Rejected',
                'created_at' => '2019-11-16 05:38:18',
                'updated_at' => '2019-11-16 05:38:18',
            ),
        ));
        
        
    }
}