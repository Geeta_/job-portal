<?php

use Illuminate\Database\Seeder;

class JobsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('jobs')->delete();

        \DB::table('jobs')->insert(array (
            0 =>
            array (
                'id' => 1,
                'cat_id' => 6,
                'type_id' => 8,
                'location_id' => 12,
                'title' => 'Senior Software Engineer - Data Management',
                'description' => '## Who we are

The Data Management Organization is tasked with ownership and management of the

shared services data environments. This function oversees the shared data services

environment and ensures company-wide adherence to the data lifecycle, privacy,

security, and ultimately the data strategy.

## What You\'ll Do

Build internal systems driving the effectiveness of Twitter’s shared data environment,

work closely with team members and stakeholders to build new and extend existing

systems. Your focus will be on building scalable backend systems for our applications,

but with the opportunity to work on other parts of the stack as well. This role reports to the

Director of Data Management.

## JOB DESCRIPTION

- 5+ years of experience as Back-end Engineer
- Ability to take on complex problems, learn quickly, and persist towards a good solution
- Strong technical background with experience with
- Scripting languages: Bash, Python, Java
- Build Tools: Git, Gradle, Selenium Tests, Chef
- Monitoring Tools: Sumo, Prometheus, Graphana, OpsGenie, etc
- Multi-threaded performance testing with Java and Python
- Backend server technologies like Scala, TLS, Finagle, Finatra, and Flask
- A detailed approach to prototyping, writing tests and quality assurance',
                'salary' => 90000,
                'active' => 1,
                'expired_at' => '2020-12-20 00:00:00',
                'created_at' => '2019-07-28 14:57:33',
                'updated_at' => '2019-11-16 10:13:51',
            ),
            1 =>
            array (
                'id' => 2,
                'cat_id' => 5,
                'type_id' => 8,
                'location_id' => 13,
            'title' => 'Finance Project Manager (PMO)',
            'description' => '**The Finance Project Management Office (PMO)** team is looking for an energetic, pro-active Project Manager to join the Finance Operations team. The candidate will work with Finance business users and IT Team members to design, build, and support various applications across multiple projects in areas of Order to Cash, Credit & Collections, FP&A and integrations with Product and Sales. The ideal candidate should establish positive, professional relationships with both business and IT teams, is a trusted partner and a respectful challenger, and acts as the primary liaison between the business and the IT teams for the project. The individual must be a strategic thinker while having the proven ability to work well with the entire organization to ensure consistency of process across all departments and countries.



## Who We Are:

- Work with cross functional teams such as OTC, Credit & Collections, FP&A, Revenue product and Sales Operations for various enhancements and project asks.
- Manage day-to-day operational aspects of projects to ensure project documents are complete, current, and signed-off by all stakeholders.
- Manage the entire project lifecycle from project definition through implementation, partnering with Finance , Oracle ERP IT and IT Product Managers. Develop project plan and drive project milestones.
- Engage with Global business process owners to define business requirements with IT, drafting use cases/user stories, conditions of satisfaction, KPIs and cut-over activities.
- Drive operational excellence into process and procedures, with a focus on documentation, cross-training, testing, monitoring, and measurement.
- Establish proper communication channels with various business functions and respond appropriately to all business concerns.
- Provides day-to-day direction to project resources. Responsible for preparation of project documentation, and status reports.
- Accountable for meeting agreed upon scope, schedule and quality measures.
- Recognize possible solutions to business problems and help select the most appropriate solution. Challenges current business practices and proposed solutions to ensure the best alternative is being selected.
- Keep abreast of changes in the company and proactively engages in identifying solutions for future changes.
- Comply with corporate policies and procedures, and other compliance areas.


## Who You Are:

- 7+ years of experience working in the capacity of a Project Manager
- Experience working on major ERP and CRM systems
- Experience with global end-to-end O2C, Collections and Financial Planning and reporting good to have.
- Experience with all aspects of IT system implementation including, but not limited to as-is and to-be business process mapping, requirements gathering, design, development, testing, cut-over and go-live activities.
- Experience developing and executing complex project plans.
- Experience in building and leading a high performance from both internal and external teams.
- Excellent written, interpersonal and communication skills
- Expert knowledge of MS Project, Excel, MS Word and Visio.
- BS in business, finance, systems, engineering or project management
- PMP Certification desirable
- You exhibit an enthusiastic, can-do spirit and approach your work with curiosity, resilience, and a growth mindset in a fast-paced, dynamic, and often ambiguous work environment.
- You have a demonstrated commitment to ongoing personal growth and development.
- You are passionate about technology and relish the opportunity to learn and apply new technical concepts to your work.',
                'salary' => 95000,
                'active' => 1,
                'expired_at' => '2020-12-20 00:00:00',
                'created_at' => '2019-07-28 15:16:29',
                'updated_at' => '2019-11-16 10:16:14',
            ),
            2 =>
            array (
                'id' => 3,
                'cat_id' => 7,
                'type_id' => 9,
                'location_id' => 14,
                'title' => 'Global Business Brand Team - Campaign Activations',
                'description' => '## Team

Codedott’s Global Business Brand Team is responsible for driving awareness and consideration of Codedott for advertisers through a variety of bold, provocative marketing initiatives including global advertising campaigns, thought leadership and insights marketing, global B2B event positioning and content, and sales training and presentations. Our team drives strategy, storytelling and execution of innovative programs that break through to our target audience, set the bar for creative excellence in the industry and achieve results in support of our revenue team’s goals.

## Role

The Brand Campaign Activations Lead will be responsible for driving our global brand campaign, related content programs and experiential activations with a focus on the periods pre-during-post Cannes and CES. They will lead the strategic planning that sets a foundation for creative excellence and act as a consultant on brand campaign executions across regions. They will have strong experience developing global creative strategies, managing campaigns, and working with creative and media agencies.

## Responsibilities

- Lead global campaign strategy, communicating our core business value proposition and key benefits for advertisers around the world
- Partner with our internal creative team as well as external creative and media agencies to develop bold, provocative work and elevate ideas that drive outsized visibility and impact
- Foster innovation in how our business brand breaks through to a highly discerning audience of the world’s top media planners, creatives and buyers. Work with thought leaders to explore new platforms for storytelling
- Drive consistency in strategy and message and act as a partner to cross functional partners and regional marketing teams
- Serve as a trusted leader for the team and within the broader marketing org

## Requirements

Bachelor’s Degree.
- 7-8+ years experience working on high functioning marketing and / or agency teams
- A big thinker who can take multiple, evolving inputs, and condense to simple, powerful positioning and messaging
- Strong analytical capabilities, with an ability to unearth insights, and tell stories through data.
- Ability to write very clear, focused briefs
- Proven experience leading strategy for large / influential brands, and helping these brands grow
- Experience in leading integrated global brand campaigns
- Experience working with and managing creative and media agencies
- Comfort with public speaking and pitching ideas to senior leadership
- Outstanding communication, teamwork and interpersonal skills; #OneTeam mentality
- Growth mindset; always looking to learn, improve and understand
- Strong organizational and time management skills; proficient at balancing multiple workstreams and projects
- Excels in fast-paced and dynamic environment; track record of working successfully across complex cross-functional teams',
                'salary' => 99000,
                'active' => 1,
                'expired_at' => '2020-12-20 00:00:00',
                'created_at' => '2019-07-28 15:21:12',
                'updated_at' => '2019-11-17 13:00:25',
            ),
            3 =>
            array (
                'id' => 4,
                'cat_id' => 6,
                'type_id' => 10,
                'location_id' => 15,
            'title' => 'Customer Engineer (Data Management)',
                'description' => 'The abc Cloud Platform team helps customers transform and evolve their business through the use of abc’s global network, web-scale data centers and software infrastructure. As part of an entrepreneurial team in this rapidly growing business, you will help shape the future of businesses of all sizes use technology to connect with customers, employees and partners.

## Minimum qualifications:

- Bachelor\'s degree in Computer Science or equivalent practical experience.
- SQL experience with query troubleshooting experience. Experience with database technologies.
- Experience with database performance tuning including network performance, storage performance, and database tuning techniques.
- Experience as a technical sales engineer in a cloud computing environment or equivalent experience in a customer-facing role.

## Preferred qualifications:

- Master’s or PhD degree in Engineering, Computer Science or other technical related field, or equivalent practical experience.
- 4 years of technical sales experience or professional consulting experience in the fields of systems integration, large scale data transfer/management, and enterprise database performance.
- Strong experience with optimizing database performance with respect to transactional and/or analytic workloads
- Deep expertise in disaster recovery (DR) and data backup strategies
- Ability to learn quickly learn, understand, and work with new emerging technologies, methodologies, and solutions in the Cloud/IT Technology space.
- Ability to effectively present to both technical and non-technical audiences.',
                'salary' => 85000,
                'active' => 1,
                'expired_at' => '2020-12-20 00:00:00',
                'created_at' => '2019-07-28 15:48:26',
                'updated_at' => '2019-11-17 12:59:57',
            ),
            4 =>
            array (
                'id' => 5,
                'cat_id' => 5,
                'type_id' => 8,
                'location_id' => 14,
                'title' => 'Anti-Money Laundering Transaction Monitoring Manager',
                'description' => 'ABC\'s mission is to give people the power to build community and bring the world closer together. Through our family of apps and services, we\'re building a different kind of company that connects billions of people around the world, gives them ways to share what matters most to them, and helps bring people closer together. Whether we\'re creating new products or helping a small business expand its reach, people at ABC are builders at heart. Our global teams are constantly iterating, solving problems, and working together to empower people around the world to build community and connect in meaningful ways. Together, we can help people build stronger communities — we\'re just getting started.

As a member of the ABC Payments Compliance Team, you will be leading a team and executing on large-scale projects in Payments Compliance area. You will be a mentor and coach to the team members and be part of the Global Payments Compliance team. You will interact with other Team Leads to continually help drive process innovation and operational excellence across all processes in the Payments Compliance area. At times, you will work collaboratively with a dynamic group of cross-functional stakeholders including but not limited to Legal, Product, Engineering, Finance and Marketing. You may have side projects to help create innovative scalable AML compliance systems and processes to harness the "transactional" power of ABC, Instagram, WhatsApp, Messenger and other ABC companies. You will help ABC Payments ensure that its compliance policies, internal controls and systems help ABC in its mission to "make the world more open and connected."
ABC Payments Inc. (“ABC Payments”) is committed to complying with Anti-Money Laundering/Counter-Terrorist Financing (“AML”/“CTF”) obligations imposed by federal, state, foreign, and other applicable laws. These governing laws and regulations may include without limitation the following: the Bank Secrecy Act (“BSA”); the USA PATRIOT Act of 2001 (“Patriot Act”); the rules and regulations overseen by the United States Department of the Treasury’s Office of Foreign Assets Control (“OFAC”); and other requirements applicable to registered money services businesses (“MSBs”). ABC Payments maintains a risk-based Policy and Program designed to ensure compliance by ABC Payments and its affiliates with governing law.

## RESPONSIBILITIES

- Promoting a "culture of compliance" to ensure efficient and effective risk and compliance management practices by adhering to required standards and processes.
- Support AML program requirements related to controlling risk to the company and to our "communities". This includes operating new internal controls and working with AML systems and processes to mitigate these risks.
- Adapt to regulatory changes or emerging industry best practices.
- Provide input as a team lead to various risk assessments for our AML/CTF Program.
- Ensure we are meeting our objectives and key results for the department and help the department achieve its key performance indicators (KPI’s).
- From time to time you may be asked to help provide various compliance training on policy and operations training relating to the AML/CTF Program.
- Be a team member on a project or operational basis that may include cross-functional teams and/or vendors.

## MINIMUM QUALIFICATIONS

- Bachelor\'s degree
- 6+ years’ experience in a compliance department or other internal control function at a financial services company in an operational function
- 2+ years’ experience in managing teams of two or more
- Experience as a team lead on cross-functional projects demonstrating impact, and influencing and leading organizational change
- Experience interacting with other process leads, cross-functional teams and leaders of the department
- Experience building relationships and experience working collaboratively as part of a team',
                'salary' => 95000,
                'active' => 1,
                'expired_at' => '2020-12-20 00:00:00',
                'created_at' => '2019-07-28 15:53:30',
                'updated_at' => '2019-11-17 12:59:25',
            ),
        ));


    }
}
