<?php

use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('options')->delete();

        \DB::table('options')->insert(array (
            0 =>
            array (
                'id' => 1,
                'key' => 'app_name',
                'value' => 'Recruitment',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'key' => 'app_url',
                'value' => 'http://yourdomain.com',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'key' => 'app_locale',
                'value' => 'en',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'key' => 'email_notification',
                'value' => 'notify@test.com',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'key' => 'mail_driver',
                'value' => 'smtp',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 =>
            array (
                'id' => 6,
                'key' => 'mail_host',
                'value' => 'smtp.mailtrap.io',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 =>
            array (
                'id' => 7,
                'key' => 'mail_port',
                'value' => '2525',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 =>
            array (
                'id' => 8,
                'key' => 'mail_username',
                'value' => 'null',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 =>
            array (
                'id' => 9,
                'key' => 'mail_password',
                'value' => 'null',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 =>
            array (
                'id' => 10,
                'key' => 'mail_encryption',
                'value' => 'null',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 =>
            array (
                'id' => 11,
                'key' => 'mail_from_name',
                'value' => 'Recruitment',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 =>
            array (
                'id' => 12,
                'key' => 'mail_from_address',
                'value' => 'hello@example.com',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 =>
            array (
                'id' => 13,
                'key' => 'en.pagination.previous',
                'value' => 'Previous',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 =>
            array (
                'id' => 14,
                'key' => 'en.pagination.next',
                'value' => 'Next',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 =>
            array (
                'id' => 15,
                'key' => 'en.miscellaneous.add_another',
                'value' => 'Add Another',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 =>
            array (
                'id' => 16,
                'key' => 'en.miscellaneous.continue_editing',
                'value' => 'Continue Editing',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 =>
            array (
                'id' => 17,
                'key' => 'en.miscellaneous.cover_letter',
                'value' => 'Cover Letter',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 =>
            array (
                'id' => 18,
                'key' => 'en.miscellaneous.delete_confirmation_message',
                'value' => 'Are you sure you want to delete this resource? | Are you sure you want to delete the selected resources?',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 =>
            array (
                'id' => 19,
                'key' => 'en.miscellaneous.delete_resource',
                'value' => 'Delete Resource | Delete Resources',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 =>
            array (
                'id' => 20,
                'key' => 'en.miscellaneous.expire_at',
                'value' => 'Expire At',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 =>
            array (
                'id' => 21,
                'key' => 'en.miscellaneous.hiring_process',
                'value' => 'Hiring Process',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 =>
            array (
                'id' => 22,
                'key' => 'en.miscellaneous.list_view',
                'value' => 'List View',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 =>
            array (
                'id' => 23,
                'key' => 'en.miscellaneous.new_applicant',
                'value' => 'New Applicant | New Applicants',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 =>
            array (
                'id' => 24,
                'key' => 'en.miscellaneous.no_record',
                'value' => 'No record matched the given criteria.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 =>
            array (
                'id' => 25,
                'key' => 'en.miscellaneous.update_process',
                'value' => 'Update Process',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 =>
            array (
                'id' => 26,
                'key' => 'en.miscellaneous.visit_site',
                'value' => 'Visit Site',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 =>
            array (
                'id' => 27,
                'key' => 'en.words.applicant',
                'value' => 'Applicant | Applicants',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 =>
            array (
                'id' => 28,
                'key' => 'en.words.cancel',
                'value' => 'Cancel',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 =>
            array (
                'id' => 29,
                'key' => 'en.words.category',
                'value' => 'Category | Categories',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 =>
            array (
                'id' => 30,
                'key' => 'en.words.create',
                'value' => 'Create',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 =>
            array (
                'id' => 31,
                'key' => 'en.words.dashboard',
                'value' => 'Dashboard',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 =>
            array (
                'id' => 32,
                'key' => 'en.words.delete',
                'value' => 'Delete',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 =>
            array (
                'id' => 33,
                'key' => 'en.words.description',
                'value' => 'Description',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 =>
            array (
                'id' => 34,
                'key' => 'en.words.edit',
                'value' => 'Edit',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 =>
            array (
                'id' => 35,
                'key' => 'en.words.email',
                'value' => 'Email',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 =>
            array (
                'id' => 36,
                'key' => 'en.words.general',
                'value' => 'General',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 =>
            array (
                'id' => 37,
                'key' => 'en.words.hired',
                'value' => 'Hired',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 =>
            array (
                'id' => 38,
                'key' => 'en.words.job',
                'value' => 'Job | Jobs',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 =>
            array (
                'id' => 39,
                'key' => 'en.words.kanban',
                'value' => 'Kanban',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 =>
            array (
                'id' => 40,
                'key' => 'en.words.language',
                'value' => 'Language | Languages',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 =>
            array (
                'id' => 41,
                'key' => 'en.words.location',
                'value' => 'Location | Locations',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 =>
            array (
                'id' => 42,
                'key' => 'en.words.logout',
                'value' => 'Logout',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 =>
            array (
                'id' => 43,
                'key' => 'en.words.name',
                'value' => 'Name | Names',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 =>
            array (
                'id' => 44,
                'key' => 'en.words.other',
                'value' => 'Other | Others',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 =>
            array (
                'id' => 45,
                'key' => 'en.words.password',
                'value' => 'Password',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 =>
            array (
                'id' => 46,
                'key' => 'en.words.permission',
                'value' => 'Permission | Permissions',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 =>
            array (
                'id' => 47,
                'key' => 'en.words.phone',
                'value' => 'Phone',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 =>
            array (
                'id' => 48,
                'key' => 'en.words.process',
                'value' => 'Process | Processes',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 =>
            array (
                'id' => 49,
                'key' => 'en.words.rejected',
                'value' => 'Rejected',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 =>
            array (
                'id' => 50,
                'key' => 'en.words.resource',
                'value' => 'Resource | Resources',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 =>
            array (
                'id' => 51,
                'key' => 'en.words.resume',
                'value' => 'Resume',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 =>
            array (
                'id' => 52,
                'key' => 'en.words.role',
                'value' => 'Role | Roles',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 =>
            array (
                'id' => 53,
                'key' => 'en.words.run',
                'value' => 'Run',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 =>
            array (
                'id' => 54,
                'key' => 'en.words.salary',
                'value' => 'Salary',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 =>
            array (
                'id' => 55,
                'key' => 'en.words.save',
                'value' => 'Save',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 =>
            array (
                'id' => 56,
                'key' => 'en.words.search',
                'value' => 'Search',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 =>
            array (
                'id' => 57,
                'key' => 'en.words.setting',
                'value' => 'Setting | Settings',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 =>
            array (
                'id' => 58,
                'key' => 'en.words.status',
                'value' => 'Status',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 =>
            array (
                'id' => 59,
                'key' => 'en.words.title',
                'value' => 'Title | Titles',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 =>
            array (
                'id' => 60,
                'key' => 'en.words.type',
                'value' => 'Type | Types',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 =>
            array (
                'id' => 61,
                'key' => 'en.words.update',
                'value' => 'Update | Updates',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            61 =>
            array (
                'id' => 62,
                'key' => 'en.words.user',
                'value' => 'User | Users',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            62 =>
            array (
                'id' => 63,
                'key' => 'fr.pagination.previous',
                'value' => 'Previous',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            63 =>
            array (
                'id' => 64,
                'key' => 'fr.pagination.next',
                'value' => 'Next',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            64 =>
            array (
                'id' => 65,
                'key' => 'fr.miscellaneous.add_another',
                'value' => 'Add Another',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            65 =>
            array (
                'id' => 66,
                'key' => 'fr.miscellaneous.continue_editing',
                'value' => 'Continue Editing',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            66 =>
            array (
                'id' => 67,
                'key' => 'fr.miscellaneous.cover_letter',
                'value' => 'Cover Letter',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            67 =>
            array (
                'id' => 68,
                'key' => 'fr.miscellaneous.delete_confirmation_message',
                'value' => 'Are you sure you want to delete this resource? | Are you sure you want to delete the selected resources?',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            68 =>
            array (
                'id' => 69,
                'key' => 'fr.miscellaneous.delete_resource',
                'value' => 'Delete Resource | Delete Resources',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            69 =>
            array (
                'id' => 70,
                'key' => 'fr.miscellaneous.expire_at',
                'value' => 'Expire At',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            70 =>
            array (
                'id' => 71,
                'key' => 'fr.miscellaneous.hiring_process',
                'value' => 'Hiring Process',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            71 =>
            array (
                'id' => 72,
                'key' => 'fr.miscellaneous.list_view',
                'value' => 'List View',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            72 =>
            array (
                'id' => 73,
                'key' => 'fr.miscellaneous.new_applicant',
                'value' => 'New Applicant | New Applicants',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            73 =>
            array (
                'id' => 74,
                'key' => 'fr.miscellaneous.no_record',
                'value' => 'No record matched the given criteria.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            74 =>
            array (
                'id' => 75,
                'key' => 'fr.miscellaneous.update_process',
                'value' => 'Update Process',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            75 =>
            array (
                'id' => 76,
                'key' => 'fr.miscellaneous.visit_site',
                'value' => 'Visit Site',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            76 =>
            array (
                'id' => 77,
                'key' => 'fr.words.applicant',
                'value' => 'Applicant | Applicants',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            77 =>
            array (
                'id' => 78,
                'key' => 'fr.words.cancel',
                'value' => 'Cancel',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            78 =>
            array (
                'id' => 79,
                'key' => 'fr.words.category',
                'value' => 'Category | Categories',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            79 =>
            array (
                'id' => 80,
                'key' => 'fr.words.create',
                'value' => 'Create',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            80 =>
            array (
                'id' => 81,
                'key' => 'fr.words.dashboard',
                'value' => 'Dashboard',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            81 =>
            array (
                'id' => 82,
                'key' => 'fr.words.delete',
                'value' => 'Delete',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            82 =>
            array (
                'id' => 83,
                'key' => 'fr.words.description',
                'value' => 'Description',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            83 =>
            array (
                'id' => 84,
                'key' => 'fr.words.edit',
                'value' => 'Edit',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            84 =>
            array (
                'id' => 85,
                'key' => 'fr.words.email',
                'value' => 'Email',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            85 =>
            array (
                'id' => 86,
                'key' => 'fr.words.general',
                'value' => 'General',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            86 =>
            array (
                'id' => 87,
                'key' => 'fr.words.hired',
                'value' => 'Hired',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            87 =>
            array (
                'id' => 88,
                'key' => 'fr.words.job',
                'value' => 'Job | Jobs',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            88 =>
            array (
                'id' => 89,
                'key' => 'fr.words.kanban',
                'value' => 'Kanban',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            89 =>
            array (
                'id' => 90,
                'key' => 'fr.words.language',
                'value' => 'Language | Languages',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            90 =>
            array (
                'id' => 91,
                'key' => 'fr.words.location',
                'value' => 'Location | Locations',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            91 =>
            array (
                'id' => 92,
                'key' => 'fr.words.logout',
                'value' => 'Logout',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            92 =>
            array (
                'id' => 93,
                'key' => 'fr.words.name',
                'value' => 'Name | Names',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            93 =>
            array (
                'id' => 94,
                'key' => 'fr.words.other',
                'value' => 'Other | Others',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            94 =>
            array (
                'id' => 95,
                'key' => 'fr.words.password',
                'value' => 'Password',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            95 =>
            array (
                'id' => 96,
                'key' => 'fr.words.permission',
                'value' => 'Permission | Permissions',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            96 =>
            array (
                'id' => 97,
                'key' => 'fr.words.phone',
                'value' => 'Phone',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            97 =>
            array (
                'id' => 98,
                'key' => 'fr.words.process',
                'value' => 'Process | Processes',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            98 =>
            array (
                'id' => 99,
                'key' => 'fr.words.rejected',
                'value' => 'Rejected',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            99 =>
            array (
                'id' => 100,
                'key' => 'fr.words.resource',
                'value' => 'Resource | Resources',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            100 =>
            array (
                'id' => 101,
                'key' => 'fr.words.resume',
                'value' => 'Resume',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            101 =>
            array (
                'id' => 102,
                'key' => 'fr.words.role',
                'value' => 'Role | Roles',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            102 =>
            array (
                'id' => 103,
                'key' => 'fr.words.run',
                'value' => 'Run',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            103 =>
            array (
                'id' => 104,
                'key' => 'fr.words.salary',
                'value' => 'Salary',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            104 =>
            array (
                'id' => 105,
                'key' => 'fr.words.save',
                'value' => 'Save',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            105 =>
            array (
                'id' => 106,
                'key' => 'fr.words.search',
                'value' => 'Search',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            106 =>
            array (
                'id' => 107,
                'key' => 'fr.words.setting',
                'value' => 'Setting | Settings',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            107 =>
            array (
                'id' => 108,
                'key' => 'fr.words.status',
                'value' => 'Status',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            108 =>
            array (
                'id' => 109,
                'key' => 'fr.words.title',
                'value' => 'Title | Titles',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            109 =>
            array (
                'id' => 110,
                'key' => 'fr.words.type',
                'value' => 'Type | Types',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            110 =>
            array (
                'id' => 111,
                'key' => 'fr.words.update',
                'value' => 'Update | Updates',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            111 =>
            array (
                'id' => 112,
                'key' => 'fr.words.user',
                'value' => 'User | Users',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            112 =>
            array (
                'id' => 113,
                'key' => 'jobs_apply_terms',
                'value' => 'I hereby certify that the above information is correct.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));


    }
}
