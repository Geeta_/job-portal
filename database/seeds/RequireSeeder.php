<?php

use Illuminate\Database\Seeder;
use Laranext\Authorization\Role;

class RequireSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->permissions();
        $this->categories();
        $this->process();
        $this->users();
        $this->options();
        $this->languages();
        $this->defaultLanguage();

        $roles = [
            [
                'name' => 'Admin',
            ],
            [
                'name' => 'Manager',
            ],
        ];

        foreach ($roles as $role) {
            $role = Role::create($role);

            if ($role['name'] == 'Manager') {
                $role->assignPermission([
                    'job:view',
                    'category:view',
                    'type:view',
                    'location:view',
                    'process:view',
                    'applicant:view',
                    'applicant:update',
                ]);
            }
            else {
                $role->assignPermission([
                    'category:view',
                    'category:show',
                    'category:create',
                    'category:update',
                    'category:delete',
                    'applicant:view',
                    'applicant:show',
                    'applicant:create',
                    'applicant:update',
                    'applicant:delete',
                    'job:view',
                    'job:show',
                    'job:create',
                    'job:update',
                    'job:delete',
                    'type:view',
                    'type:show',
                    'type:create',
                    'type:update',
                    'type:delete',
                    'location:view',
                    'location:show',
                    'location:create',
                    'location:update',
                    'location:delete',
                    'user:view',
                    'user:show',
                    'user:create',
                    'user:update',
                    'user:delete',
                    'process:view',
                    'process:show',
                    'process:create',
                    'process:update',
                    'process:delete',
                    'authorization:view',
                    'authorization:show',
                    'authorization:create',
                    'authorization:update',
                    'authorization:delete',
                    'setting',
                ]);
            }
        }
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected function users()
    {
        $users = [
            [
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'password' => bcrypt('admin'),
            ],
        ];

        DB::table('users')->insert($users);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected function categories()
    {
        $categories = [
            [
                'name' => 'Job Category',
                'key' => 'Job Category'
            ],
            [
                'name' => 'Job Type',
                'key' => 'Job Type'
            ],
            [
                'name' => 'Job Location',
                'key' => 'Job Location'
            ],
            [
                'name' => 'Hiring Process',
                'key' => 'Hiring Process'
            ],
        ];

        DB::table('categories')->insert($categories);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected function process()
    {
        $process = [
            [
                'parent_id' => 4,
                'name' => 'Applied',
                'slug' => 'applied',
                'key' => 'Applied',
            ],
            [
                'parent_id' => 4,
                'name' => 'Hired',
                'slug' => 'hired',
                'key' => 'Hired',
            ],
            [
                'parent_id' => 4,
                'name' => 'Rejected',
                'slug' => 'rejected',
                'key' => 'Rejected',
            ],
        ];

        DB::table('categories')->insert($process);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected function options()
    {
        $options = [
            [
                'key' => 'app_name',
                'value' => 'Recruitment',
            ],
            [
                'key' => 'app_url',
                'value' => 'http://yourdomain.com',
            ],
            [
                'key' => 'app_locale',
                'value' => 'en',
            ],
            [
                'key' => 'email_notification',
                'value' => 'notify@test.com',
            ],
            [
                'key' => 'mail_driver',
                'value' => 'smtp',
            ],
            [
                'key' => 'mail_host',
                'value' => 'smtp.mailtrap.io',
            ],
            [
                'key' => 'mail_port',
                'value' => '2525',
            ],
            [
                'key' => 'mail_username',
                'value' => 'null',
            ],
            [
                'key' => 'mail_password',
                'value' => 'null',
            ],
            [
                'key' => 'mail_encryption',
                'value' => 'null',
            ],
            [
                'key' => 'mail_from_name',
                'value' => 'Recruitment',
            ],
            [
                'key' => 'mail_from_address',
                'value' => 'hello@example.com',
            ],
            [
                'key' => 'jobs_apply_terms',
                'value' => 'I hereby certify that the above information is correct.',
            ],
        ];

        DB::table('options')->insert($options);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            'category:view',
            'category:show',
            'category:create',
            'category:update',
            'category:delete',
            'applicant:view',
            'applicant:show',
            'applicant:create',
            'applicant:update',
            'applicant:delete',
            'job:view',
            'job:show',
            'job:create',
            'job:update',
            'job:delete',
            'type:view',
            'type:show',
            'type:create',
            'type:update',
            'type:delete',
            'location:view',
            'location:show',
            'location:create',
            'location:update',
            'location:delete',
            'user:view',
            'user:show',
            'user:create',
            'user:update',
            'user:delete',
            'process:view',
            'process:show',
            'process:create',
            'process:update',
            'process:delete',
            'authorization:view',
            'authorization:show',
            'authorization:create',
            'authorization:update',
            'authorization:delete',
            'setting',
        ];

        $data = collect($permissions)->map(function ($value) {
            return ['name' => $value];
        })->all();

        DB::table('permissions')->insert($data);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected function languages()
    {
        $languages = [
            [
                'name' => 'English',
                'code' => 'en',
                'active' => true,
            ],
            [
                'name' => 'French',
                'code' => 'fr',
                'active' => true,
            ],
            [
                'name' => 'Spanish',
                'code' => 'es',
                'active' => false,
            ],
        ];

        DB::table('languages')->insert($languages);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected function defaultLanguage()
    {
        $language = [
            'pagination.previous' => 'Previous',
            'pagination.next' => 'Next',

            'miscellaneous.add_another' => 'Add Another',
            'miscellaneous.continue_editing' => 'Continue Editing',
            'miscellaneous.cover_letter' => 'Cover Letter',
            'miscellaneous.delete_confirmation_message' => 'Are you sure you want to delete this resource? | Are you sure you want to delete the selected resources?',
            'miscellaneous.delete_resource' => 'Delete Resource | Delete Resources',
            'miscellaneous.expire_at' => 'Expire At',
            'miscellaneous.hiring_process' => 'Hiring Process',
            'miscellaneous.list_view' => 'List View',
            'miscellaneous.new_applicant' => 'New Applicant | New Applicants',
            'miscellaneous.no_record' => 'No record matched the given criteria.',
            'miscellaneous.update_process' => 'Update Process',
            'miscellaneous.visit_site' => 'Visit Site',

            'words.applicant' => 'Applicant | Applicants',
            'words.cancel' => 'Cancel',
            'words.category' => 'Category | Categories',
            'words.create' => 'Create',
            'words.dashboard' => 'Dashboard',
            'words.delete' => 'Delete',
            'words.description' => 'Description',
            'words.edit' => 'Edit',
            'words.email' => 'Email',
            'words.general' => 'General',
            'words.hired' => 'Hired',
            'words.job' => 'Job | Jobs',
            'words.kanban' => 'Kanban',
            'words.language' => 'Language | Languages',
            'words.location' => 'Location | Locations',
            'words.logout' => 'Logout',
            'words.name' => 'Name | Names',
            'words.other' => 'Other | Others',
            'words.password' => 'Password',
            'words.permission' => 'Permission | Permissions',
            'words.phone' => 'Phone',
            'words.process' => 'Process | Processes',
            'words.rejected' => 'Rejected',
            'words.resource' => 'Resource | Resources',
            'words.resume' => 'Resume',
            'words.role' => 'Role | Roles',
            'words.run' => 'Run',
            'words.salary' => 'Salary',
            'words.save' => 'Save',
            'words.search' => 'Search',
            'words.setting' => 'Setting | Settings',
            'words.status' => 'Status',
            'words.title' => 'Title | Titles',
            'words.type' => 'Type | Types',
            'words.update' => 'Update | Updates',
            'words.user' => 'User | Users',
        ];

        DB::table('options')->insert(
            collect($language)->map(function ($value, $key) {
                return ['key' => 'en.'.$key, 'value' => $value];
            })->values()->all()
        );

        DB::table('options')->insert(
            collect($language)->map(function ($value, $key) {
                return ['key' => 'fr.'.$key, 'value' => $value];
            })->values()->all()
        );
    }
}
