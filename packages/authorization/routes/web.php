<?php

use Illuminate\Support\Facades\Route;

Route::resource('roles', RolesController::class)->except(['store', 'update', 'destroy']);

Route::get('permissions', 'PermissionsController@index');
