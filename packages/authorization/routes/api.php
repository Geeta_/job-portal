<?php

Route::resource('roles', RolesController::class);

Route::get('roles/{role}/permissions', RolePermissions::class);
