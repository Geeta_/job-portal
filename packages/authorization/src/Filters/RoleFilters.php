<?php

namespace Laranext\Authorization\Filters;

use Laranext\Filters\Filters;

class RoleFilters extends Filters
{
    /**
     * Get the filters available for the resource.
     *
     * @return array
     */
    public function filters()
    {
        return [];
    }
}
