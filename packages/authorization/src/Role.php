<?php

namespace Laranext\Authorization;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Laranext\Authorization\Filters\RoleFilters;

class Role extends Model
{
    /**
     * The users that belong to the role.
     */
    public function users()
    {
        return $this->belongsToMany(User::class)->using(RoleUser::class);
    }

    /**
     * The permissions that belong to the role.
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
     * assign permission to the role.
     */
    public function assignPermission($permission)
    {
        if (is_array($permission)) {
            foreach ($permission as $value) {
                $this->permissions()->attach(
                    Permission::whereName($value)->firstOrFail()
                );
            }

            return true;
        }

        return $this->permissions()->sync(
            Permission::whereName($permission)->firstOrFail()
        );
    }

    /**
     * Apply all relevant role filters.
     *
     * @param  Builder       $query
     * @param  Laranext\Authorization\Filters\RoleFilters $filters
     * @return Builder
     */
    public function scopeFilter($query, RoleFilters $filters)
    {
        return $filters->apply($query);
    }
}
