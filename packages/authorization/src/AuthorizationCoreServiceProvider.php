<?php

namespace Laranext\Authorization;

use Laranext\Laranext;
use Illuminate\Support\ServiceProvider;

class AuthorizationCoreServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot(Authorization $authorization)
    {
        if (Laranext::runningInConsole()) {
            $this->app->register(AuthorizationServiceProvider::class);
        }

        $authorization->handle();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
