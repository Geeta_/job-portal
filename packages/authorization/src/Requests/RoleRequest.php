<?php

namespace Laranext\Authorization\Requests;

use Illuminate\Support\Facades\DB;
use Laranext\Requests\FormRequest;
use Illuminate\Support\Facades\Cache;

class RoleRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:roles,name,'.$this->model->id,
            'permissions' => 'required',
        ];
    }

    /**
     * Database Transaction.
     *
     * @return void
     */
    public function transaction()
    {
        DB::transaction(function () {
            $this->model->name = $this->attributes['name'];
            $this->model->save();

            $this->model->permissions()->sync($this->attributes['permissions']);

            Cache::flush();
        });
    }
}
