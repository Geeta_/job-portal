<?php

namespace Laranext\Authorization;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * The roles that belong to the permission.
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class)->using(PermissionRole::class);
    }
}
