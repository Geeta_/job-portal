<?php

namespace Laranext\Authorization;

use Laranext\Laranext;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AuthorizationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        if (Laranext::runningInConsole()) {
            $this->registerPublishing();
        }

        $this->registerResources();
    }

    /**
     * Register the package's publishable resources.
     *
     * @return void
     */
    protected function registerPublishing()
    {
        $this->publishes([
            __DIR__.'/../resources/views/' => resource_path('views/vendor/authorization'),
        ], 'authorization-views');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register the package resources such as routes, templates, etc.
     *
     * @return void
     */
    protected function registerResources()
    {
        $this->registerRoutes();

        config(['view.paths' => [
            Laranext::views(),
            __DIR__.'/../resources/views/',
            resource_path('views'),
        ]]);

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }

    /**
     * Register the package routes.
     *
     * @return void
     */
    protected function registerRoutes()
    {
        Route::group([
            'namespace' => 'Laranext\Authorization\Http\Controllers',
            'middleware' => ['web', 'auth'],
            'prefix' => config('laranext.admin_prefix') . '/' . Laranext::key(),
        ], function () {
            $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        });

        Route::group([
            'namespace' => 'Laranext\Authorization\Http\Controllers\Api',
            'middleware' => ['web', 'auth'],
            'prefix' => 'laranext-api/' . Laranext::key(),
        ], function () {
            $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
        });
    }
}
