<?php

namespace Laranext\Authorization\Exceptions;

use InvalidArgumentException;

class PermissionDoesNotExist extends InvalidArgumentException
{
    //
}
