<?php

namespace Laranext\Authorization\Fields;

use Laranext\Fields\Text;
use Laranext\Fields\Fields;

class PermissionFields extends Fields
{
    /**
     * Get the fields displayed by the resource.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Text::make('Permission', 'name')
                ->value($this->model->name)
                ->required(),
        ];
    }

    /**
     * Get the index fields displayed by the resource.
     *
     * @return array
     */
    public function indexFields()
    {
        return [
            Text::make(trans_choice('messages.words.permission', 2), 'name')
                ->value($this->model->name),
        ];
    }
}
