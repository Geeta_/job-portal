<?php

namespace Laranext\Authorization\Fields;

use Laranext\Fields\Text;
use Illuminate\Support\Str;
use Laranext\Fields\Fields;
use Laranext\Fields\Checkbox;
use Laranext\Authorization\Permission;

class RoleFields extends Fields
{
    /**
     * Get the fields displayed by the resource.
     *
     * @return array
     */
    public function fields()
    {
        // return [
        //     'options' => Permission::get(['id', 'name'])->groupBy(function ($item, $key) {
        //         return Str::before($item['name'], ':');
        //     })
        // ];

        return [
            Text::make(trans_choice('messages.words.role', 1), 'name')
                ->value($this->model->name)
                ->inline()
                ->required(),

            Checkbox::make(trans_choice('messages.words.permission', 2), 'permissions')
                ->options(Permission::get(['id', 'name'])->groupBy(function ($item, $key) {
                    return Str::before($item['name'], ':');
                }))
                ->value($this->model->permissions->pluck('id'))
                ->withMeta(['class' => 'w-4/5'])
                ->inline()
                ->required(),
        ];
    }

    /**
     * Get the index fields displayed by the resource.
     *
     * @return array
     */
    public function indexFields()
    {
        return [
            Text::make(trans_choice('messages.words.role', 1), 'name')
                ->value($this->model->name)
                ->sortable(),
        ];
    }
}
