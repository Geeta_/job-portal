<?php

namespace Laranext\Authorization\Http\Controllers;

use Laranext\Authorization\Role;
use App\Http\Controllers\Controller;

class RolesController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource('authorization');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        return view('roles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Laranext\Authorization\Role  $role
     * @return View
     */
    public function create(Role $role)
    {
        return view('roles.form', compact('role'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \Laranext\Authorization\Role  $role
     * @return View
     */
    public function show(Role $role)
    {
        return view('roles.detail', compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Laranext\Authorization\Role  $role
     * @return View
     */
    public function edit(Role $role)
    {
        return view('roles.form', compact('role'));
    }
}
