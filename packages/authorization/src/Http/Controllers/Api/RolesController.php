<?php

namespace Laranext\Authorization\Http\Controllers\Api;

use Illuminate\Http\Request;
use Laranext\IndexCollection;
use Laranext\Authorization\Role;
use App\Http\Controllers\Controller;
use Laranext\Authorization\Fields\RoleFields;
use Laranext\Authorization\Filters\RoleFilters;
use Laranext\Authorization\Requests\RoleRequest;

class RolesController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource('authorization');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Laranext\Authorization\Filters\RoleFilters  $filters
     * @return \Illuminate\Http\Response
     */
    public function index(RoleFilters $filters)
    {
        return new IndexCollection(
            Role::filter($filters)->simplePaginate(),
            RoleFields::class
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Laranext\Attendance\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function create(Role $role)
    {
        return new RoleFields($role);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laranext\Attendance\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Role $role)
    {
        return new RoleRequest($request, $role);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Laranext\Attendance\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        return $role;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Laranext\Attendance\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        return new RoleFields($role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laranext\Attendance\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        return new RoleRequest($request, $role);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $items = [];

        foreach ($request->items as $item) {
            $hasPermissions = Role::has('permissions')->find($item);

            if (!$hasPermissions) {
                $items[] = $item;
            }
            else {
                return ['message' => 'Please first delete permissions related to role!'];
            }
        }

        Role::destroy($request->items);

        return [
            'message' => count($request->items) > 1
                ? 'Roles Deleted Successfully!'
                : 'Role Deleted Successfully!'
        ];
    }
}
