<?php

namespace Laranext\Authorization\Http\Controllers\Api;

use Laranext\IndexCollection;
use App\Http\Controllers\Controller;
use Laranext\Authorization\Permission;
use Illuminate\Database\Eloquent\Builder;
use Laranext\Authorization\Fields\PermissionFields;

class RolePermissions extends Controller
{
    public function __construct()
    {
        $this->authorizeResource('authorization');
    }

    /**
     * Handle the incoming request.
     *
     * @param  int  $role
     * @return \Illuminate\Http\Response
     */
    public function __invoke(int $role)
    {
        return new IndexCollection(
            Permission::whereHas('roles', function (Builder $query) use ($role) {
                $query->where('id', $role);
            })->simplePaginate(),
            PermissionFields::class
        );
    }
}
