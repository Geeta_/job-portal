<?php

namespace Laranext\Authorization;

use Illuminate\Database\Eloquent\Relations\Pivot;

class PermissionRole extends Pivot
{
    // use RecordsActivity;

    public static function boot()
    {
        parent::boot();

        static::created(function ($item) {
            // dd($item);
        });
    }
}
