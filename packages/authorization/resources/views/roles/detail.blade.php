@extends('layouts.default')

@section('topbar')
    <div class="flex items-center text-sm text-gray-700 uppercase font-bold tracking-widest py-6">
        {{ trans_choice('messages.words.role', 2) }}
    </div>
@endsection

@section('content')
    <v-detail name="authorization" uri="roles" :id="{{ $role->id }}" no-actions></v-detail>

    <div class="card py-3 px-6">
        <data-row name="{{ trans_choice('messages.words.role', 1) }}" data="{{ $role->name }}"></data-row>
    </div>

    <h3 class="mt-10 mb-4 text-lg">{{ trans_choice('messages.words.permission', 2) }}</h3>

    <v-index name="permission"
             route="roles/{{ $role->id }}/permissions"
             title="{{ trans_choice('messages.words.permission', 1) }}"
             no-actions
             no-filters
             no-select
             no-topbar
             no-create
             no-table-actions
    ></v-index>
@endsection
