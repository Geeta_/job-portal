@extends('layouts.default')

@section('topbar')
    <div class="flex items-center text-sm text-gray-700 uppercase font-bold tracking-widest py-6">
        {{ trans_choice('messages.words.role', 2) }}
    </div>
@endsection

@section('content')
    <v-index name="role"
             route="roles"
             title="{{ trans_choice('messages.words.role', 1) }}"
             no-actions
             no-filters
    ></v-index>
@endsection
