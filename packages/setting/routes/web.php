<?php

use Illuminate\Support\Facades\Route;

Route::get('general', GeneralSettings::class);
Route::get('email', EmailSettings::class);
