@extends('master')

@section('topbar')
    <div class="flex items-center text-sm text-gray-700 uppercase font-bold tracking-widest py-6">
        {{ trans_choice('messages.words.setting', 2) }} <span class="text-lg px-2 font-light">|</span> {{ trans_choice('messages.words.general', 1) }}
    </div>
@endsection

@section('content')
    <v-form name="setting"
            route="general"
            save-only
    ></v-form>
@endsection
