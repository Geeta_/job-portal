@extends('layouts.split-menu')

@section('menu')
    <div class="card overflow-hidden py-2 w-56">
        <a href="/{{ config('laranext.admin_prefix') }}/settings/general" class="sub-menu-item {{ isActive('general', 3) }}">General</a>
        <a href="/{{ config('laranext.admin_prefix') }}/settings/email" class="sub-menu-item {{ isActive('email', 3) }}">Email Settings</a>
    </div>
@endsection
