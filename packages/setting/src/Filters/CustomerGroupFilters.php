<?php

namespace Laranext\Setting\Filters;

use Laranext\Filters\Filters;

class CustomerGroupFilters extends Filters
{
    /**
     * Get the filters available for the resource.
     *
     * @return array
     */
    public function filters()
    {
        return [];
    }
}
