<?php

namespace Laranext\Setting\Fields;

use Laranext\Fields\Text;
use Laranext\Fields\Fields;
use Laranext\Fields\Textarea;

class GeneralFields extends Fields
{
    /**
     * Get the fields displayed by the resource.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Text::make('App Name')
                ->value(option('app_name'))
                ->inline(),

            Text::make('App Url')
                ->value(option('app_url'))
                ->inline(),

            Text::make('Email Notification')
                ->value(option('email_notification'))
                ->inline(),

            Textarea::make('Jobs Apply Terms')
                ->value(option('jobs_apply_terms'))
                ->inline(),
        ];
    }
}
