<?php

namespace Laranext\Setting\Fields;

use Laranext\Fields\Text;
use Laranext\Fields\Fields;
use Laranext\Fields\Select;

class EmailFields extends Fields
{
    /**
     * Get the fields displayed by the resource.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Select::make('Driver', 'mail_driver')
                ->value(option('mail_driver'))
                ->inline()
                ->options([
                    ['id' => 'smtp', 'name' => 'smtp']
                ]),

            Text::make('Host', 'mail_host')
                ->value(option('mail_host'))
                ->inline(),

            Text::make('Port', 'mail_port')
                ->value(option('mail_port'))
                ->inline(),

            Text::make('Username', 'mail_username')
                ->value(option('mail_username'))
                ->inline(),

            Text::make('Password', 'mail_password')
                ->value(option('mail_password'))
                ->inline(),

            Select::make('Encryption', 'mail_encryption')
                ->value(option('mail_encryption'))
                ->inline()
                ->options([
                    ['id' => 'null', 'name' => 'null'],
                    ['id' => 'tls', 'name' => 'tls'],
                    ['id' => 'ssl', 'name' => 'ssl'],
                ]),

            Text::make('From Name', 'mail_from_name')
                ->value(option('mail_from_name'))
                ->inline(),

            Text::make('From Address', 'mail_from_address')
                ->value(option('mail_from_address'))
                ->inline(),
        ];
    }
}
