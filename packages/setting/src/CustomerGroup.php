<?php

namespace Laranext\Setting;

use Laranext\Categorizable;
use Illuminate\Database\Eloquent\Model;
use Laranext\Setting\Filters\CustomerGroupFilters;

class CustomerGroup extends Model
{
    use Categorizable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The Category Parent.
     *
     * @var string
     */
    protected static $parent = 'Customer Groups';

    /**
     * Apply all relevant customer-group filters.
     *
     * @param  Builder       $query
     * @param  Laranext\Setting\CustomerGroupFilters $filters
     * @return Builder
     */
    public function scopeFilter($query, CustomerGroupFilters $filters)
    {
        return $filters->apply($query);
    }
}
