<?php

namespace Laranext\Setting\Http\Controllers\Api;

use Laranext\Option;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laranext\Setting\Fields\EmailFields;
use Laranext\Setting\Requests\EmailRequest;

class EmailSettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:setting');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Laranext\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function create(Option $option)
    {
        return new EmailFields($option);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laranext\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Option $option)
    {
        return new EmailRequest($request, $option);
    }
}
