<?php

namespace Laranext\Setting\Http\Controllers;

use App\Http\Controllers\Controller;

class GeneralSettings extends Controller
{
    public function __construct()
    {
        $this->middleware('can:setting');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function __invoke()
    {
        return view('general');
    }
}
