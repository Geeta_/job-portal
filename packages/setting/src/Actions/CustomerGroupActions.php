<?php

namespace Laranext\Setting\Actions;

use Laranext\Actions\Actions;

class CustomerGroupActions extends Actions
{
    /**
     * Get the actions available for the resource.
     *
     * @return array
     */
    public function actions()
    {
        return [];
    }
}
