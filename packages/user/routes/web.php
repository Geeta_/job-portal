<?php

use Illuminate\Support\Facades\Route;

// Route::resource('users', UsersController::class)->except(['store', 'update', 'destroy']);
Route::get('/', 'UsersController@index');
Route::get('/create', 'UsersController@create');
Route::get('/{user}', 'UsersController@show');
Route::get('/{user}/edit', 'UsersController@edit');
