<?php

use Illuminate\Support\Facades\Route;

Route::get('{resource}/actions', 'ActionsController@index');
Route::post('{resource}/action', 'ActionsController@store');
Route::get('{resource}/filters', 'FiltersController@index');

Route::get('/', 'UsersController@index');
Route::get('/create', 'UsersController@create');
Route::post('/', 'UsersController@store');
Route::get('/{user}', 'UsersController@show');
Route::get('/{user}/edit', 'UsersController@edit');
Route::patch('/{user}', 'UsersController@update');
Route::delete('/{user}', 'UsersController@destroy');
