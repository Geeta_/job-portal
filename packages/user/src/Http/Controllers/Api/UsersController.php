<?php

namespace Laranext\User\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use Laranext\IndexCollection;
use App\Http\Controllers\Controller;
use Laranext\User\Fields\UserFields;
use Laranext\User\Filters\UserFilters;
use Laranext\User\Requests\UserRequest;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource('user');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Laranext\User\Filters\UserFilters  $filters
     * @return \Illuminate\Http\Response
     */
    public function index(UserFilters $filters)
    {
        return new IndexCollection(
            User::filter($filters)->simplePaginate(),
            UserFields::class
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        return new UserFields($user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        return new UserRequest($request, $user);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return new UserFields($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        return new UserRequest($request, $user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $items = $request->items;

        if (in_array(1, $items)) {
            return ['message' => 'Sorry, we can\'t delete this user!'];
        }

        User::destroy($items);

        return [
            'message' => count($request->items) > 1
                ? 'Users Deleted Successfully!'
                : 'User Deleted Successfully!'
        ];
    }
}
