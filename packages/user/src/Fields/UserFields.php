<?php

namespace Laranext\User\Fields;

use Laranext\Fields\Text;
use Laranext\Fields\Radio;
use Laranext\Fields\Fields;
use Laranext\Fields\Password;
use Laranext\Authorization\Role;

class UserFields extends Fields
{
    /**
     * Get the fields displayed by the resource.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Text::make(trans_choice('messages.words.name', 1), 'name')
                ->value($this->model->name)
                ->required()
                ->inline(),

            Text::make(trans_choice('messages.words.email', 1), 'email')
                ->value($this->model->email)
                ->required()
                ->inline(),

            Password::make(trans_choice('messages.words.password', 1), 'password')
                ->required()
                ->inline(),

            Radio::make(trans_choice('messages.words.role', 1), 'role')
                ->value(optional(optional($this->model->roles)->first())->id)
                ->options(Role::get(['id', 'name']))
                ->required()
                ->inline(),
        ];
    }

    /**
     * Get the index fields displayed by the resource.
     *
     * @return array
     */
    public function indexFields()
    {
        $role = optional(optional($this->model->roles)->first())->name ?? '—';

        return [
            Text::make(trans_choice('messages.words.name', 1), 'name')
                ->value($this->model->name)
                ->sortable(),
            Text::make(trans_choice('messages.words.email', 1), 'email')
                ->value($this->model->email)
                ->sortable(),
            Text::make(trans_choice('messages.words.role', 1), 'role')
                ->value($role)
        ];
    }
}
