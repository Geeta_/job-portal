<?php

namespace Laranext\User\Requests;

use Illuminate\Support\Facades\DB;
use Laranext\Requests\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $this->model->id,
            'password' => 'required',
            'role' => 'required',
        ];
    }

    /**
     * Get the update validation rules that apply to the request.
     *
     * @return array
     */
    public function updateRules()
    {
        return [
            'password' => 'nullable',
        ];
    }

    /**
     * Database Transaction.
     *
     * @return void
     */
    public function transaction()
    {
        DB::transaction(function () {
            if (!$this->attributes['password']) {
                unset($this->attributes['password']);
            }

            $roles = $this->attributes['role'];
            unset($this->attributes['role']);

            $this->model->forceFill($this->attributes);

            $this->model->save();

            $this->model->roles()->sync($roles);
        });
    }
}
