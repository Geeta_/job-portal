<?php

use Illuminate\Support\Facades\Route;

Route::get('/', ShowHomepage::class);
Route::get('jobs/{job}', 'JobsController@show');
Route::get('jobs/{job}/apply', 'JobsApplyController@create');
Route::post('jobs/apply', 'JobsApplyController@store');
