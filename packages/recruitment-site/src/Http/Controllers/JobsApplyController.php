<?php

namespace Laranext\RecruitmentSite\Http\Controllers;

use App\Mail\NewApplicant;
use Illuminate\Http\Request;
use Laranext\Recruitment\Job;
use Laranext\Recruitment\Type;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Laranext\Recruitment\Category;
use Laranext\Recruitment\Applicant;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class JobsApplyController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create($id)
    {
        $types = Type::all();
        $categories = Category::all();

        $job = Job::findOrFail($id);

        return view('jobs.apply', compact('job', 'types', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $attributes = $request->validate([
            'job_id' => 'required',
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('applicants', 'email')->where(function ($query) {
                return $query->where('job_id', request('job_id'));
            })],
            'phone' => ['required', Rule::unique('applicants', 'phone')->where(function ($query) {
                return $query->where('job_id', request('job_id'));
            })],
            'resume' => 'required|mimes:pdf',
            'cover_letter' => 'required',
            'accept_terms' => 'required',
        ]);

        $attributes['resume'] = $request->file('resume')->store('resumes');
        $attributes['process_id'] = Category::getId('Applied');
        $attributes['status'] = 'Applied';
        unset($attributes['accept_terms']);

        DB::transaction(function () use ($attributes) {
            $applicant = Applicant::create($attributes);

            Mail::to(option('email_notification'))->send(new NewApplicant($applicant));
        });

        return back()->with('flash', 'Your request has been received! Thank you!');
    }
}
