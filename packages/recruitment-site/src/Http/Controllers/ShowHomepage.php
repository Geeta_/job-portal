<?php

namespace Laranext\RecruitmentSite\Http\Controllers;

use Laranext\Recruitment\Job;
use Laranext\Recruitment\Type;
use Laranext\Recruitment\Category;
use App\Http\Controllers\Controller;
use Laranext\Recruitment\Filters\JobFilters;

class ShowHomepage extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(JobFilters $filters)
    {
        $types = Type::all();
        $categories = Category::all();

        $jobs = Job::where('title', 'LIKE', '%' . request('title') . '%')
                    ->whereDate('expired_at', '>', now())
                    ->filter($filters)
                    ->paginate();

        return view('home', get_defined_vars());
    }
}
