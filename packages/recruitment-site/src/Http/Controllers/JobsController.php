<?php

namespace Laranext\RecruitmentSite\Http\Controllers;

use Illuminate\Http\Request;
use Laranext\Recruitment\Job;
use Laranext\Recruitment\Type;
use Laranext\Recruitment\Category;
use App\Http\Controllers\Controller;

class JobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $types = Type::all();
        $categories = Category::all();

        if ($request->title) {
            $job = Job::with('category', 'location')
                        ->latest()
                        ->where('title', 'LIKE', '%' . $request->title . '%');

            if ($request->category) {
                $job->where('cat_id', $request->category);
            }

            if ($request->type) {
                $job->where('type_id', $request->type);
            }

            $jobs = $job->paginate();
        }
        elseif ($request->type && $request->category) {
            $jobs = Job::with('category', 'location')->where('cat_id', $request->category)->where('type_id', $request->type)->latest()->paginate();
        }
        elseif ($request->type) {
            $jobs = Job::with('category', 'location')->where('type_id', $request->type)->latest()->paginate();
        }
        elseif ($request->category) {
            $jobs = Job::with('category', 'location')->where('cat_id', $request->category)->latest()->paginate();
        }
        else {
            $jobs = Job::with('category', 'location')->latest()->paginate();
        }

        return view('jobs.index', compact('jobs', 'types', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\View\View
     */
    public function show(Job $job)
    {
        $types = Type::all();
        $categories = Category::all();

        return view('jobs.show', compact('job', 'types', 'categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
