<?php

namespace Laranext\RecruitmentSite;

use Laranext\Laranext;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class RecruitmentSiteServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerResources();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register the package resources such as routes, templates, etc.
     *
     * @return void
     */
    protected function registerResources()
    {
        $this->registerRoutes();

        config(['view.paths' => [
            __DIR__.'/../resources/views/custom',
            __DIR__.'/../resources/views/default',
            resource_path('views'),
        ]]);
    }

    /**
     * Register the package routes.
     *
     * @return void
     */
    protected function registerRoutes()
    {
        Route::group([
            'namespace' => 'Laranext\RecruitmentSite\Http\Controllers',
            'middleware' => 'web',
            'prefix' => Laranext::prefix() . '/' . Laranext::key(),
        ], function () {
            $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        });
    }
}
