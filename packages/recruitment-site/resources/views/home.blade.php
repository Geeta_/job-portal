@extends('layouts.master')

@section('content')
    <div class="page-hero page-hero-lg" style="background-image: url(/vendor/specialty/images/hero-1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-hero-content">
                        <h2 class="page-title">
                            <span class="text-theme">Work there.</span> Find the dream job
                            <br> you’ve always wanted.
                        </h2>
                    </div>
                </div>
            </div>
        </div>

        <form action="/" class="form-filter">
            <div class="form-filter-header">
                <a href="#" class="form-filter-dismiss">&times;</a>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-xs-12">
                        <label for="job-title" class="sr-only">Job Title</label>
                        <input type="text" id="job-title" placeholder="Title" name="title" value="{{ request('title') }}">
                    </div>
                    <div class="col-lg-3 col-xs-12">
                        <label for="job-type" class="sr-only">Job Type</label>
                        <div class="ci-select">
                            <select id="job-type" name="type">
                                <option value="">Any Type</option>
                                @foreach($types as $type)
                                    <option value="{{ $type->id }}" {{ request('type') == $type->id ? 'selected' : '' }}>{{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-12">
                        <label for="job-category" class="sr-only">Job Category</label>
                        <div class="ci-select">
                            <select id="job-category" name="category">
                                <option value="">Any Category</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" {{ request('category') == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-12">
                        <button class="btn btn-block" type="submit">Search</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <main class="main">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 col-lg-8 col-xs-12">
                    <h3 class="section-title">
                        <b>{{ $jobs->total() }}</b> Jobs Found</h3>

                    <div class="item-listing">
                        @foreach($jobs as $job)
                            <div class="list-item">
                                <div class="list-item-main-info">
                                    <p class="list-item-title">
                                        <a href="/jobs/{{ $job->id }}">{{ $job->title }}</a>
                                    </p>

                                    <div class="list-item-meta">
                                        <a href="" class="list-item-tag item-badge job-type-contract">{{ $job->category->name }}</a>
                                        <span class="list-item-company">{{ $job->type->name }}</span>
                                    </div>
                                </div>

                                <div class="list-item-secondary-info">
                                    <p class="list-item-location">{{ $job->location->name }}</p>
                                    <time class="list-item-time" datetime="2017-01-01">{{ $job->created_at->diffForHumans() }}</time>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    {{ $jobs->appends($_GET)->links() }}
                </div>

                <div class="col-xl-3 col-lg-4 col-xs-12">
                    <div class="sidebar-wrap">
                        <div class="sidebar-wrap-header">
                            <a href="#" class="sidebar-wrap-dismiss">&times;</a>
                        </div>

                        <div class="sidebar">
                            @include('layouts.sidebar')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
