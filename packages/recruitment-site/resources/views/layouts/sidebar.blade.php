<div class="widget widget_ci-filters-widget">
    <h3 class="widget-title">Job Type</h3>
    <ul class="item-filters-array">
        @foreach($types as $type)
        <li class="item-filter">
            <input type="checkbox" id="type-{{ $type->id }}" class="checkbox-filter" {{ request('type') == $type->id ? 'checked' : '' }}>
            <label class="checkbox-filter-label" for="type-{{ $type->id }}" onclick="type({{ $type->id }})">
                <span class="item-filter-tag item-filter-tag-badge">
                    {{ $type->name }}
                    <span class="item-filter-tag-bg"></span>
                </span>
            </label>
        </li>
        @endforeach
    </ul>
</div>

<div class="widget widget_ci-filters-widget">
    <h3 class="widget-title">Job Category</h3>
    <ul class="item-filters-array">
        @foreach($categories as $category)
            <li class="item-filter">
                <input type="checkbox" id="category-{{ $category->id }}" class="checkbox-filter" {{ request('category') == $category->id ? 'checked' : '' }}>
                <label class="checkbox-filter-label" for="category-{{ $category->id }}" onclick="category({{ $category->id }})">
                    <span class="item-filter-tag item-filter-tag-badge">
                        {{ $category->name }}
                        <span class="item-filter-tag-bg"></span>
                    </span>
                </label>
            </li>
        @endforeach
    </ul>
</div>
