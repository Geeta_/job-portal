<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>Home &ndash; Recruitment</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/vendor/specialty/css/base.css">
    <link rel="stylesheet" type="text/css" href="/vendor/specialty/css/mmenu.css">
    <link rel="stylesheet" type="text/css" href="/vendor/specialty/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/vendor/specialty/css/magnific.css">
    <link rel="stylesheet" type="text/css" href="/vendor/specialty/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="/vendor/specialty/style.css">

    <link rel="shortcut icon" href="#">
    <link rel="apple-touch-icon" href="#">
    <link rel="apple-touch-icon" sizes="72x72" href="#">
    <link rel="apple-touch-icon" sizes="114x114" href="#">
</head>
<body>
    <div id="page">
        @include('layouts.header')

        @yield('content')

        <div class="mobile-triggers">
            <a href="#" class="mobile-trigger form-filter-trigger">
                <i class="fa fa-search"></i> Search
            </a>

            <a href="#" class="mobile-trigger sidebar-wrap-trigger">
                <i class="fa fa-navicon"></i> Filters
            </a>
        </div>

        @include('layouts.footer')
    </div>
    <!-- #page -->

    <script src="/vendor/specialty/js/jquery-1.12.3.min.js"></script>
    <script src="/vendor/specialty/js/jquery.mmenu.min.all.js"></script>
    <script src="/vendor/specialty/js/jquery.fitvids.js"></script>
    <script src="/vendor/specialty/js/jquery.magnific-popup.js"></script>
    <script src="/vendor/specialty/js/jquery.matchHeight.js"></script>
    <script src="/vendor/specialty/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="/vendor/specialty/js/scripts.js"></script>

    <script type="text/javascript">
        function category(id) {
            var urlParams = new URLSearchParams(window.location.search)

            if (urlParams.get('type'))
                window.location.href = '/?category=' + id + '&type=' + urlParams.get('type')
            else
                window.location.href = '/?category=' + id
        }

        function type(id) {
            var urlParams = new URLSearchParams(window.location.search)

            if (urlParams.get('category'))
                window.location.href = '/?type=' + id + '&category=' + urlParams.get('category')
            else
                window.location.href = '/?type=' + id
        }
    </script>

    <!-- Default Statcounter code for
    Recruitment-demo.codedott.com
    http://recruitment-demo.codedott.com -->
    <script type="text/javascript">
    var sc_project=12144798;
    var sc_invisible=1;
    var sc_security="e7b1eb89";
    </script>
    <script type="text/javascript"
    src="https://www.statcounter.com/counter/counter.js"
    async></script>
    <noscript><div class="statcounter"><a title="Web Analytics
    Made Easy - StatCounter" href="https://statcounter.com/"
    target="_blank"><img class="statcounter"
    src="https://c.statcounter.com/12144798/0/e7b1eb89/1/"
    alt="Web Analytics Made Easy -
    StatCounter"></a></div></noscript>
    <!-- End of Statcounter Code -->

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5e1c1dcb27773e0d832d3a50/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
</body>
</html>
