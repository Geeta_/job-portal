<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="footer-copy">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <p>
                                © 2020 codedott. All Rights Reserved.
                            </p>
                        </div>

                        <div class="col-sm-6 col-xs-12 text-right">
                            <p>Powered by
                                <a href="http://codedott.com" target="_blank">codedott</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
