@extends('layouts.master')

@section('content')
    <div class="page-hero" style="background-image: url(/vendor/specialty/images/hero-1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-hero-content">
                        <h1 class="page-title">{{ $job->title }}</h1>
                        <div class="page-hero-details">
                            <span class="item-badge job-type-full-time">{{ $job->category->name }}</span>
                            <span class="entry-location">{{ $job->location->name }}</span>
                            <span class="entry-company">{{ $job->type->name }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <main class="main main-elevated">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 col-lg-8 col-xs-12">
                    <div class="content-wrap">
                        <article class="entry markdown-body">
                            <div class="entry-content">
                                @markdown($job->description)
                            </div>
                        </article>

                        <a href="/jobs/{{ $job->id }}/apply" class="btn btn-block btn-apply-content">Apply for this job</a>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-4 col-xs-12">
                    <div class="sidebar">
                        <aside class="widget widget_ci-apply-button-widget">
                            <a href="/jobs/{{ $job->id }}/apply" class="btn btn-block">
                                Apply for this job
                            </a>
                        </aside>

                        @include('layouts.sidebar')
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
