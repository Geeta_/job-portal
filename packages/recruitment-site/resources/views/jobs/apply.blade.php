@extends('layouts.master')

@section('content')
    <div class="page-hero" style="background-image: url(/vendor/specialty/images/hero-1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-hero-content">
                        <h1 class="page-title">{{ $job->title }}</h1>
                        <div class="page-hero-details">
                            <span class="item-badge job-type-full-time">{{ $job->category->name }}</span>
                            <span class="entry-location">{{ $job->location->name }}</span>
                            <span class="entry-company">{{ $job->type->name }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <main class="main main-elevated">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 col-lg-8 col-xs-12">
                    <div class="content-wrap">
                        <article class="entry">
                            <div class="entry-content">
                                @if(session('flash'))
                                    <div class="box-message box-message-success">
                                        <div class="box-message-content">
                                            <p>{{ session('flash') }}</p>
                                        </div>
                                    </div>
                                @endif

                                @if($errors->first())
                                    <div class="box-message box-message-error">
                                        <div class="box-message-content">
                                            <p>Some errors occured!</p>

                                            <ul class="box-message-errors">
                                                @foreach($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endif

                                <form method="POST" action="/jobs/apply" class="form form-submit-listing" enctype="multipart/form-data">
                                    @csrf

                                    <input type="hidden" name="job_id" value="{{ $job->id }}">

                                    <div class="form-field form-field-inline">
                                        <label for="name">Name <span style="color: red;">*</span></label>

                                        <div class="field">
                                            <input type="text" id="name" name="name" value="{{ old('name') }}">
                                        </div>
                                    </div>

                                    <div class="form-field form-field-inline">
                                        <label for="email">Email <span style="color: red;">*</span></label>

                                        <div class="field">
                                            <input type="text" id="email" name="email" value="{{ old('email') }}">
                                        </div>
                                    </div>

                                    <div class="form-field form-field-inline">
                                        <label for="marital_status">Marital Status <span style="color: red;">*</span></label>

                                        <div class="field">
                                            <select id="marital_status" name="marital_status" style="width: 100%;">
                                                <option value="single" {{ old('marital_status') == 'single' ? 'selected' : '' }}>Single</option>
                                                <option value="married" {{ old('marital_status') == 'married' ? 'selected' : '' }}>Married</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-field form-field-inline">
                                        <label for="national_id">National ID <span style="color: red;">*</span></label>

                                        <div class="field">
                                            <input type="text" id="national_id" name="national_id" value="{{ old('national_id') }}">
                                        </div>
                                    </div>

                                    <div class="form-field form-field-inline">
                                        <label for="phone">Phone <span style="color: red;">*</span></label>

                                        <div class="field">
                                            <input type="text" id="phone" name="phone" value="{{ old('phone') }}">
                                        </div>
                                    </div>

                                    <div class="form-field form-field-inline">
                                        <label for="birthday">Birthday <span style="color: red;">*</span></label>

                                        <div class="field">
                                            <input type="date" id="birthday" name="birthday" value="{{ old('birthday') }}">
                                        </div>
                                    </div>

                                    <div class="form-field form-field-inline">
                                        <label for="address">Address <span style="color: red;">*</span></label>

                                        <div class="field">
                                            <input type="text" id="address" name="address" value="{{ old('address') }}">
                                        </div>
                                    </div>

                                    <div class="form-field form-field-inline">
                                        <label for="resume">C.V or Resume</label>

                                        <div class="field">
                                            <input type="file" id="resume" name="resume" value="{{ old('resume') }}">
                                        </div>
                                    </div>

                                    <div class="form-field form-field-inline">
                                        <label for="cover_letter">Cover Letter <span style="color: red;">*</span></label>

                                        <div class="field">
                                            <textarea id="cover_letter" cols="10" rows="10" name="cover_letter">{{ old('cover_letter') }}</textarea>
                                            <span class="field-hint">* You may use Markdown with <a href="https://help.github.com/en/articles/basic-writing-and-formatting-syntax">GitHub-flavored</a></span>
                                        </div>
                                    </div>

                                    <div class="form-field form-field-inline">
                                        <label><span style="color: red;">*</span></label>

                                        <div class="field">
                                            <input type="checkbox" name="accept_terms" style="margin-right: 5px;">
                                            <span>
                                                {{ option('jobs_apply_terms') }}
                                            </span>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn">Submit</button>
                                </form>
                            </div>
                        </article>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-4 col-xs-12">
                    <div class="sidebar">
                        @include('layouts.sidebar')
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
