@extends('layouts.default')

@section('topbar')
    <div class="flex items-center text-sm text-gray-700 uppercase font-bold tracking-widest py-6">
        Types
    </div>
@endsection

@section('content')
    <div class="flex">
        @if(Auth::user()->can('type:create') || (Auth::user()->can('type:update') && $type->id))
            <div class="w-1/3 pr-6">
                <v-form name="type"
                        route="types"
                        title="Type"
                        id="{{ $type->id }}"
                        samepage
                ></v-form>
            </div>
        @endif

        <div class="{{ (Auth::user()->can('type:create') || (Auth::user()->can('type:update') && $type->id)) ? 'w-2/3' : 'flex-1' }}">
            <v-index name="type"
                     route="types"
                     title="Type"
                     no-actions
                     no-filters
                     no-create
                     no-view
            ></v-index>
        </div>
    </div>
@endsection
