@extends('layouts.default')

@section('topbar')
    <div class="flex items-center text-sm text-gray-700 uppercase font-bold tracking-widest py-6">
        Hiring Process
    </div>
@endsection

@section('content')
    <div class="flex">
        @if(Auth::user()->can('process:create') || (Auth::user()->can('process:update') && $process->id))
            <div class="w-1/3 pr-6">
                <v-form name="process"
                        route="processes"
                        title="Process"
                        id="{{ $process->id }}"
                        samepage
                ></v-form>
            </div>
        @endif

        <div class="{{ (Auth::user()->can('process:create') || (Auth::user()->can('process:update') && $process->id)) ? 'w-2/3' : 'flex-1' }}">
            <v-index name="process"
                     route="processes"
                     title="Process"
                     no-actions
                     no-filters
                     no-create
                     no-view
            ></v-index>
        </div>
    </div>
@endsection
