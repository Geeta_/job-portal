@extends('layouts.default')

@push('styles')
    <link href="/css/github-markdown.css" rel="stylesheet">
@endpush

@section('topbar')
    <div class="flex items-center text-sm text-gray-700 uppercase font-bold tracking-widest py-6">
        {{ trans_choice('messages.words.job', 2) }}
    </div>
@endsection

@section('content')
    <v-detail uri="jobs" :id="{{ $job->id }}" no-actions></v-detail>

    <div class="card py-3 px-6">
        <data-row name="{{ trans_choice('messages.words.title', 1) }}" data="{{ $job->title }}"></data-row>
        <data-row name="{{ trans_choice('messages.words.category', 1) }}" data="{{ $job->category->name }}"></data-row>
        <data-row name="{{ trans_choice('messages.words.location', 1) }}" data="{{ $job->location->name }}"></data-row>
        <data-row name="{{ trans_choice('messages.words.type', 1) }}" data="{{ $job->type->name }}"></data-row>
        <data-row name="{{ trans_choice('messages.words.salary', 1) }}" data="{{ $job->salary }}"></data-row>
        <data-row name="{{ trans_choice('messages.words.applicant', 2) }}" data="{{ $job->applicants->count() }}"></data-row>
        <data-row name="{{ trans_choice('messages.miscellaneous.expire_at', 1) }}" data="{{ $job->expired_at->format('M d, Y') }}"></data-row>

        <div class="flex border-gray-300 data-row">
            <div class="w-1/4 py-4">
                <h4 class="font-normal text-gray-600">{{ trans_choice('messages.words.description', 1) }}</h4>
            </div>
            <div class="w-3/4 py-4 markdown-body">
                @markdown($job->description)
            </div>
        </div>
    </div>
@endsection
