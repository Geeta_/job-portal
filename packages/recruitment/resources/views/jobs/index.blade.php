@extends('layouts.default')

@section('topbar')
    <div class="flex items-center text-sm text-gray-700 uppercase font-bold tracking-widest py-6">
        Jobs
    </div>
@endsection

@section('content')
    <v-index name="job"
             route="jobs"
             title="Job"
             no-actions
    ></v-index>
@endsection
