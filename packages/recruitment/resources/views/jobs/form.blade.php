@extends('layouts.default')

@section('topbar')
    <div class="flex items-center text-sm text-gray-700 uppercase font-bold tracking-widest py-6">
        Jobs
    </div>
@endsection

@section('content')
    <v-form name="job"
            route="jobs"
            title="Job"
            id="{{ $job->id }}"
    ></v-form>
@endsection
