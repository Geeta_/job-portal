@extends('layouts.default')

@section('topbar')
    <div class="flex items-center text-sm text-gray-700 uppercase font-bold tracking-widest py-6">
        Categories
    </div>
@endsection

@section('content')
    <div class="flex">
        @if(Auth::user()->can('category:create') || (Auth::user()->can('category:update') && $category->id))
            <div class="w-1/3 pr-6">
                <v-form name="category"
                        route="categories"
                        title="Category"
                        id="{{ $category->id }}"
                        samepage
                ></v-form>
            </div>
        @endif

        <div class="{{ (Auth::user()->can('category:create') || (Auth::user()->can('category:update') && $category->id)) ? 'w-2/3' : 'flex-1' }}">
            <v-index name="category"
                     route="categories"
                     title="Category"
                     no-actions
                     no-filters
                     no-create
                     no-view
            ></v-index>
        </div>
    </div>
@endsection
