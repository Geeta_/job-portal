@extends('layouts.default')

@section('topbar')
    <div class="flex items-center text-sm text-gray-700 uppercase font-bold tracking-widest py-6">
        Categories
    </div>
@endsection

@section('content')
    <v-form name="category"
            uri="recruitment/categories"
            title="Category"
            id="{{ $category->id }}"
    ></v-form>
@endsection
