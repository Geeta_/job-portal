@extends('layouts.default')

@section('topbar')
    <div class="flex items-center text-sm text-gray-700 uppercase font-bold tracking-widest py-6">
        {{ trans_choice('messages.words.user', 2) }}
    </div>
@endsection

@section('content')
    <v-detail name="user" uri="users" :id="{{ $user->id }}" no-actions></v-detail>

    <div class="card py-3 px-6">
        <data-row name="{{ trans_choice('messages.words.name', 1) }}" data="{{ $user->name }}"></data-row>
        <data-row name="{{ trans_choice('messages.words.email', 1) }}" data="{{ $user->email }}"></data-row>
        <data-row name="{{ trans_choice('messages.words.role', 1) }}" data="{{ optional(optional($user->roles)->first())->name }}"></data-row>
    </div>
@endsection
