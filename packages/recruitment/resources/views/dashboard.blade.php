@extends('layouts.default')

@section('content')
    <div class="flex -mx-2 mt-4">
        <div class="w-1/3 px-2">
            <div class="card px-4 py-6">
                <div class="flex">
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" class="fill-current text-blue-400"><path fill="none" d="M0 0h24v24H0V0z"/><path d="M19 3h-4.18C14.4 1.84 13.3 1 12 1s-2.4.84-2.82 2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7 0c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1zm1 14H8c-.55 0-1-.45-1-1s.45-1 1-1h5c.55 0 1 .45 1 1s-.45 1-1 1zm3-4H8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1s-.45 1-1 1zm0-4H8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1s-.45 1-1 1z"/></svg>

                    <div class="ml-6">
                        <h3 class="font-bold text-gray-600">{{ trans_choice('messages.miscellaneous.new_applicant', 2) }}</h3>
                        <h2 class="text-4xl mt-2">{{ $metrics->newApplicants() }}</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="w-1/3 px-2">
            <div class="card px-4 py-6">
                <div class="flex">
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" class="fill-current text-blue-400"><path fill="none" d="M0 0h24v24H0V0z"/><path d="M15 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm-9-2V8c0-.55-.45-1-1-1s-1 .45-1 1v2H2c-.55 0-1 .45-1 1s.45 1 1 1h2v2c0 .55.45 1 1 1s1-.45 1-1v-2h2c.55 0 1-.45 1-1s-.45-1-1-1H6zm9 4c-2.67 0-8 1.34-8 4v1c0 .55.45 1 1 1h14c.55 0 1-.45 1-1v-1c0-2.66-5.33-4-8-4z"/></svg>

                    <div class="ml-6">
                        <h3 class="font-bold text-gray-600">{{ trans_choice('messages.words.hired', 1) }}</h3>
                        <h2 class="text-4xl mt-2">{{ $metrics->hired() }}</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="w-1/3 px-2">
            <div class="card px-4 py-6">
                <div class="flex">
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" class="fill-current text-blue-400"><path fill="none" d="M0 0h24v24H0V0z"/><path d="M14.48 11.95c.17.02.34.05.52.05 2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4c0 .18.03.35.05.52l3.43 3.43zm2.21 2.21l5.74 5.74c.33-.17.57-.5.57-.9v-1c0-2.14-3.56-3.5-6.31-3.84zM2.12 2.42c-.39-.39-1.02-.39-1.41 0-.39.39-.39 1.02 0 1.41L4 7.12V10H2c-.55 0-1 .45-1 1s.45 1 1 1h2v2c0 .55.45 1 1 1s1-.45 1-1v-2h2.88l2.51 2.51C9.19 15.11 7 16.3 7 18v1c0 .55.45 1 1 1h8.88l3.29 3.29c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L2.12 2.42zM6 10v-.88l.88.88H6z"/></svg>

                    <div class="ml-6">
                        <h3 class="font-bold text-gray-600">{{ trans_choice('messages.words.rejected', 1) }}</h3>
                        <h2 class="text-4xl mt-2">{{ $metrics->rejected() }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @can('applicant:view')
        <h1 class="page-title mt-8">{{ trans_choice('messages.miscellaneous.new_applicant', 2) }}</h1>

        <div class="mt-4">
            <v-index name="applicant"
                     route="applicants?process_id=16"
                     title="{{ trans_choice('messages.words.applicant', 1) }}"
                     no-actions
                     no-filters
                     no-edit
                     no-delete
                     no-create
                     no-select
            ></v-index>
        </div>
    @endcan
@endsection
