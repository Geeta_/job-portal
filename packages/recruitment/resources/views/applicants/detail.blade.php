@extends('layouts.default')

@push('styles')
    <link rel="stylesheet" type="text/css" href="/css/github-markdown.css">
@endpush

@section('topbar')
    <div class="flex items-center text-sm text-gray-700 uppercase font-bold tracking-widest py-6">
        {{ trans_choice('messages.words.applicant', 2) }}
    </div>
@endsection

@section('content')
    <div class="flex mb-3">
        <form class="ml-auto" method="POST" action="/app/recruitment/applicants/{{ $applicant->id }}">
            @csrf
            @method('patch')

            <div class="flex items-center">
                <select class="form-select h-9 mr-2" style="min-width: 172px; line-height: 1;" required name="process">
                    @foreach($processes as $process)
                        <option value="{{ $process->id }}" {{ $applicant->process_id == $process->id ? 'selected' : '' }}>{{ $process->name }}</option>
                    @endforeach
                </select>

                <button class="btn btn-primary h-9" type="submit">
                    {{ trans_choice('messages.words.update', 1) }}
                </button>
            </div>
        </form>
    </div>

    <div class="card py-3 px-6">
        <data-row name="{{ trans_choice('messages.words.name', 1) }}" data="{{ $applicant->name }}"></data-row>
        <data-row name="{{ trans_choice('messages.words.email', 1) }}" data="{{ $applicant->email }}"></data-row>
        <data-row name="{{ trans_choice('messages.words.phone', 1) }}" data="{{ $applicant->phone }}"></data-row>
        <data-row name="{{ trans_choice('messages.words.status', 1) }}" data="{{ $applicant->process->name }}"></data-row>

        @if ($applicant->resume)
            <data-row name="{{ trans_choice('messages.words.resume', 1) }}" data="View Resume" href="/{{ $applicant->resume }}"></data-row>
        @endif

        <div class="flex border-gray-300 data-row">
            <div class="w-1/4 py-4">
                <h4 class="font-normal text-gray-600">{{ trans_choice('messages.miscellaneous.cover_letter', 1) }}</h4>
            </div>
            <div class="w-3/4 py-4 markdown-body">
                @markdown($applicant->cover_letter)
            </div>
        </div>
    </div>

    <h3 class="page-title mt-8 mb-4">Notes</h3>

    <notes id="{{ $applicant->id }}"></notes>
@endsection
