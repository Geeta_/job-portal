@extends('layouts.default')

@section('topbar')
    <div class="flex items-center text-sm text-gray-700 uppercase font-bold tracking-widest py-6">
        {{ trans_choice('messages.words.applicant', 2) }}
    </div>

    <nav class="flex py-6 text-sm">
        <a href="/app/recruitment/applicants" class="text-gray-600 hover:underline {{ request()->has('list') ?: 'text-gray-900 font-semibold hover:no-underline' }}">
            {{ __('messages.words.kanban') }}
        </a>
        <a href="/app/recruitment/applicants?list" class="text-gray-600 hover:underline pl-6 {{ request()->has('list') ? 'text-gray-900 font-semibold hover:no-underline' : '' }}">
            {{ __('messages.miscellaneous.list_view') }}
        </a>
    </nav>
@endsection

@section('content')
    <kanban></kanban>
@endsection
