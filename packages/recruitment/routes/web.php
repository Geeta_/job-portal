<?php

use Illuminate\Support\Facades\Route;

Route::get('/', ShowDashboard::class);
Route::resource('types', TypesController::class)->only(['index', 'show', 'edit']);
Route::resource('categories', CategoriesController::class)->only(['index', 'show', 'edit']);
Route::resource('locations', LocationsController::class)->only(['index', 'show', 'edit']);
Route::resource('processes', ProcessesController::class)->only(['index', 'show', 'edit']);
Route::resource('jobs', JobsController::class)->except(['store', 'update', 'destroy']);
Route::resource('applicants', ApplicantsController::class)->except(['store', 'destroy']);
