<?php

use Illuminate\Support\Facades\Route;

Route::get('{resource}/actions', 'ActionsController@index');
Route::post('{resource}/action', 'ActionsController@store');
Route::get('{resource}/filters', 'FiltersController@index');

Route::resource('types', TypesController::class);
Route::resource('categories', CategoriesController::class);
Route::resource('locations', LocationsController::class);
Route::resource('processes', ProcessesController::class);
Route::resource('jobs', JobsController::class);
Route::resource('applicants', ApplicantsController::class);

Route::post('applicants/{applicant}/process/{process}', ApplicantProcessUpdate::class);
Route::get('applicant-notes/{applicant}', 'ApplicantNotesController@index');
Route::post('applicant-notes/{applicant}', 'ApplicantNotesController@store');
Route::delete('applicant-notes/{note}', 'ApplicantNotesController@destroy');
