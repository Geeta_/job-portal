<?php

namespace Laranext\Recruitment;

use Illuminate\Database\Eloquent\Model;
use Laranext\Recruitment\Filters\JobFilters;

class Job extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'expired_at',
    ];

    /**
     * Get the category that owns the job.
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'cat_id');
    }

    /**
     * Get the location that owns the job.
     */
    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    /**
     * The type that belong to the job.
     */
    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    /**
     * Get the applicants for the job.
     */
    public function applicants()
    {
        return $this->hasMany(Applicant::class);
    }

    /**
     * Apply all relevant job filters.
     *
     * @param  Builder       $query
     * @param  Laranext\Recruitment\JobFilters $filters
     * @return Builder
     */
    public function scopeFilter($query, JobFilters $filters)
    {
        return $filters->apply($query);
    }
}
