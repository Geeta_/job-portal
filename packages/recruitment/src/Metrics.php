<?php

namespace Laranext\Recruitment;

use Illuminate\Database\Eloquent\Builder;

class Metrics
{
    public function newApplicants()
    {
        return Applicant::whereHas('process', function (Builder $query) {
            $query->where('name', 'Applied');
        })->count();
    }

    public function hired()
    {
        return Applicant::whereHas('process', function (Builder $query) {
            $query->where('name', 'Hired');
        })->count();
    }

    public function rejected()
    {
        return Applicant::whereHas('process', function (Builder $query) {
            $query->where('name', 'Rejected');
        })->count();
    }
}
