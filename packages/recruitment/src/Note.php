<?php

namespace Laranext\Recruitment;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    /**
     * The process that belong to the applicant.
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
