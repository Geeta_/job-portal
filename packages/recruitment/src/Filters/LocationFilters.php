<?php

namespace Laranext\Recruitment\Filters;

use Laranext\Filters\Filters;

class LocationFilters extends Filters
{
    /**
     * Get the filters available for the resource.
     *
     * @return array
     */
    public function filters()
    {
        return [];
    }
}
