<?php

namespace Laranext\Recruitment\Filters;

use Laranext\Filters\Filter;
use Laranext\Recruitment\Category;

class JobCategory extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * The attribute / column name of the field.
     *
     * @var string
     */
    public $attribute = 'category';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply($query, $value)
    {
        return $query->where('cat_id', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @return array
     */
    public function options()
    {
        return Category::pluck('id', 'name');
    }
}
