<?php

namespace Laranext\Recruitment\Filters;

use Laranext\Filters\Filter;
use Laranext\Recruitment\Process;

class ApplicantStatus extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * The attribute / column name of the field.
     *
     * @var string
     */
    public $attribute = 'process_id';

    /**
     * Get the displayable name of the filter.
     *
     * @return string
     */
    public function name()
    {
        return trans_choice('messages.words.status', 1);
    }

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply($query, $value)
    {
        return $query->where('process_id', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @return array
     */
    public function options()
    {
        return Process::pluck('id', 'name');
    }
}
