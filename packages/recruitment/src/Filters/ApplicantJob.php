<?php

namespace Laranext\Recruitment\Filters;

use Laranext\Filters\Filter;
use Laranext\Recruitment\Job;

class ApplicantJob extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * The attribute / column name of the field.
     *
     * @var string
     */
    public $attribute = 'job_id';

    /**
     * Get the displayable name of the filter.
     *
     * @return string
     */
    public function name()
    {
        return trans_choice('messages.words.job', 1);
    }

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply($query, $value)
    {
        return $query->where('job_id', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @return array
     */
    public function options()
    {
        return Job::pluck('id', 'title');
    }
}
