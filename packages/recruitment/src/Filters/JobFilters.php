<?php

namespace Laranext\Recruitment\Filters;

use Laranext\Filters\Filters;

class JobFilters extends Filters
{
    /**
     * Get the filters available for the resource.
     *
     * @return array
     */
    public function filters()
    {
        return [
            new JobType,
            new JobCategory,
        ];
    }
}
