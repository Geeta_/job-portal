<?php

namespace Laranext\Recruitment\Http\Controllers\Api;

use Illuminate\Http\Request;
use Laranext\IndexCollection;
use Laranext\Recruitment\Process;
use App\Http\Controllers\Controller;
use Laranext\Recruitment\Filters\ProcessFilters;
use Laranext\Recruitment\Fields\ProcessFields;
use Laranext\Recruitment\Requests\ProcessRequest;

class ProcessesController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource('process');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Laranext\Recruitment\Filters\ProcessFilters  $filters
     * @return \Illuminate\Http\Response
     */
    public function index(ProcessFilters $filters)
    {
        if (request()->has('applicants')) {
            return Process::with('applicants.job')->get();
        }

        return new IndexCollection(
            Process::filter($filters)->simplePaginate(),
            ProcessFields::class
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Laranext\Recruitment\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function create(Process $process)
    {
        return new ProcessFields($process);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laranext\Recruitment\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Process $process)
    {
        return new ProcessRequest($request, $process);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Laranext\Recruitment\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function show(Process $process)
    {
        return $process;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Laranext\Recruitment\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function edit(Process $process)
    {
        return new ProcessFields($process);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laranext\Recruitment\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Process $process)
    {
        return new ProcessRequest($request, $process);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $items = [];

        foreach ($request->items as $item) {
            $hasJobs = Process::has('applicants')->find($item);

            if ($hasJobs) {
                return ['message' => 'Please first delete applicants related to process!'];
            }
        }

        foreach ($request->items as $item) {
            $hasKey = Process::find($item);

            if (!$hasKey->key) {
                $items[] = $item;
            }
            else {
                return ['message' => 'Unable to delete core process!'];
            }
        }

        Process::destroy($items);

        return [
            'message' => count($request->items) > 1
                ? 'Processs Deleted Successfully!'
                : 'Process Deleted Successfully!'
        ];
    }
}
