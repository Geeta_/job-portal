<?php

namespace Laranext\Recruitment\Http\Controllers\Api;

use Laranext\Recruitment\Applicant;
use App\Http\Controllers\Controller;

class ApplicantProcessUpdate extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Applicant $applicant, $process)
    {
        $applicant->update([
            'process_id' => $process
        ]);

        return 'done';
    }
}
