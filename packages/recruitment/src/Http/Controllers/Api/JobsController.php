<?php

namespace Laranext\Recruitment\Http\Controllers\Api;

use Illuminate\Http\Request;
use Laranext\IndexCollection;
use Laranext\Recruitment\Job;
use App\Http\Controllers\Controller;
use Laranext\Recruitment\Filters\JobFilters;
use Laranext\Recruitment\Fields\JobFields;
use Laranext\Recruitment\Requests\JobRequest;

class JobsController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource('job');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Laranext\Recruitment\Filters\JobFilters  $filters
     * @return \Illuminate\Http\Response
     */
    public function index(JobFilters $filters)
    {
        return new IndexCollection(
            Job::filter($filters)->simplePaginate(),
            JobFields::class
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Laranext\Recruitment\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function create(Job $job)
    {
        return new JobFields($job);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laranext\Recruitment\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Job $job)
    {
        return new JobRequest($request, $job);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Laranext\Recruitment\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job)
    {
        return $job;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Laranext\Recruitment\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function edit(Job $job)
    {
        return new JobFields($job);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laranext\Recruitment\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Job $job)
    {
        return new JobRequest($request, $job);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Job::destroy($request->items);

        return [
            'message' => count($request->items) > 1
                ? 'Jobs Deleted Successfully!'
                : 'Job Deleted Successfully!'
        ];
    }
}
