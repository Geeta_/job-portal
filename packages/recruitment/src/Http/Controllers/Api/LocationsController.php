<?php

namespace Laranext\Recruitment\Http\Controllers\Api;

use Illuminate\Http\Request;
use Laranext\IndexCollection;
use Laranext\Recruitment\Location;
use App\Http\Controllers\Controller;
use Laranext\Recruitment\Filters\LocationFilters;
use Laranext\Recruitment\Fields\LocationFields;
use Laranext\Recruitment\Requests\LocationRequest;

class LocationsController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource('location');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Laranext\Recruitment\Filters\LocationFilters  $filters
     * @return \Illuminate\Http\Response
     */
    public function index(LocationFilters $filters)
    {
        return new IndexCollection(
            Location::filter($filters)->simplePaginate(),
            LocationFields::class
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Laranext\Recruitment\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function create(Location $location)
    {
        return new LocationFields($location);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laranext\Recruitment\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Location $location)
    {
        return new LocationRequest($request, $location);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Laranext\Recruitment\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        return $location;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Laranext\Recruitment\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        return new LocationFields($location);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laranext\Recruitment\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        return new LocationRequest($request, $location);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $items = [];

        foreach ($request->items as $item) {
            $hasJobs = Location::has('jobs')->find($item);

            if (!$hasJobs) {
                $items[] = $item;
            }
            else {
                return ['message' => 'Please first delete jobs related to location!'];
            }
        }

        Location::destroy($request->items);

        return [
            'message' => count($request->items) > 1
                ? 'Locations Deleted Successfully!'
                : 'Location Deleted Successfully!'
        ];
    }
}
