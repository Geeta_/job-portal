<?php

namespace Laranext\Recruitment\Http\Controllers\Api;

use Illuminate\Http\Request;
use Laranext\IndexCollection;
use Laranext\Recruitment\Category;
use App\Http\Controllers\Controller;
use Laranext\Recruitment\Filters\CategoryFilters;
use Laranext\Recruitment\Fields\CategoryFields;
use Laranext\Recruitment\Requests\CategoryRequest;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource('category');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Laranext\Recruitment\Filters\CategoryFilters  $filters
     * @return \Illuminate\Http\Response
     */
    public function index(CategoryFilters $filters)
    {
        return new IndexCollection(
            Category::filter($filters)->simplePaginate(),
            CategoryFields::class
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Laranext\Recruitment\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category)
    {
        return new CategoryFields($category);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laranext\Recruitment\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Category $category)
    {
        return new CategoryRequest($request, $category);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Laranext\Recruitment\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return $category;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Laranext\Recruitment\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return new CategoryFields($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laranext\Recruitment\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        return new CategoryRequest($request, $category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $items = [];

        foreach ($request->items as $item) {
            $hasJobs = Category::has('jobs')->find($item);

            if (!$hasJobs) {
                $items[] = $item;
            }
            else {
                return ['message' => 'Please first delete jobs related to category!'];
            }
        }

        Category::destroy($items);

        return [
            'message' => count($request->items) > 1
                ? 'Categorys Deleted Successfully!'
                : 'Category Deleted Successfully!'
        ];
    }
}
