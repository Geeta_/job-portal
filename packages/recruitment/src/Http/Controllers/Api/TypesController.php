<?php

namespace Laranext\Recruitment\Http\Controllers\Api;

use Illuminate\Http\Request;
use Laranext\IndexCollection;
use Laranext\Recruitment\Type;
use App\Http\Controllers\Controller;
use Laranext\Recruitment\Filters\TypeFilters;
use Laranext\Recruitment\Fields\TypeFields;
use Laranext\Recruitment\Requests\TypeRequest;

class TypesController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource('type');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Laranext\Recruitment\Filters\TypeFilters  $filters
     * @return \Illuminate\Http\Response
     */
    public function index(TypeFilters $filters)
    {
        return new IndexCollection(
            Type::filter($filters)->simplePaginate(),
            TypeFields::class
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Laranext\Recruitment\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function create(Type $type)
    {
        return new TypeFields($type);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laranext\Recruitment\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Type $type)
    {
        return new TypeRequest($request, $type);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Laranext\Recruitment\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function show(Type $type)
    {
        return $type;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Laranext\Recruitment\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function edit(Type $type)
    {
        return new TypeFields($type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laranext\Recruitment\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Type $type)
    {
        return new TypeRequest($request, $type);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $items = [];

        foreach ($request->items as $item) {
            $hasJobs = Type::has('jobs')->find($item);

            if (!$hasJobs) {
                $items[] = $item;
            }
            else {
                return ['message' => 'Please first delete jobs related to type!'];
            }
        }

        Type::destroy($request->items);

        return [
            'message' => count($request->items) > 1
                ? 'Types Deleted Successfully!'
                : 'Type Deleted Successfully!'
        ];
    }
}
