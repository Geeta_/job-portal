<?php

namespace Laranext\Recruitment\Http\Controllers\Api;

use Illuminate\Http\Request;
use Laranext\Recruitment\Note;
use App\Http\Controllers\Controller;

class ApplicantNotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        return Note::where('applicant_id', $id)->with('author')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $applicantId
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $applicantId)
    {
        $request->validate([
            'note' => 'required'
        ]);

        Note::create([
            'user_id' => auth()->id(),
            'applicant_id' => $applicantId,
            'note' => $request->note,
        ]);

        return 'done';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function edit(Note $note)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Note $note)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function destroy(Note $note)
    {
        $note->delete();

        return [
            'message' => 'Note Deleted Successfully!'
        ];
    }
}
