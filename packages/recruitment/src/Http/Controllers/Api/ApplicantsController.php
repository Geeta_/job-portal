<?php

namespace Laranext\Recruitment\Http\Controllers\Api;

use Illuminate\Http\Request;
use Laranext\IndexCollection;
use Laranext\Recruitment\Applicant;
use App\Http\Controllers\Controller;
use Laranext\Recruitment\Filters\ApplicantFilters;
use Laranext\Recruitment\Fields\ApplicantFields;
use Laranext\Recruitment\Requests\ApplicantRequest;

class ApplicantsController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource('applicant');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Laranext\Recruitment\Filters\ApplicantFilters  $filters
     * @return \Illuminate\Http\Response
     */
    public function index(ApplicantFilters $filters)
    {
        return new IndexCollection(
            Applicant::filter($filters)->simplePaginate(),
            ApplicantFields::class
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Laranext\Recruitment\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
    public function create(Applicant $applicant)
    {
        return new ApplicantFields($applicant);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laranext\Recruitment\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Applicant $applicant)
    {
        return new ApplicantRequest($request, $applicant);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Laranext\Recruitment\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
    public function show(Applicant $applicant)
    {
        return $applicant;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Laranext\Recruitment\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
    public function edit(Applicant $applicant)
    {
        return new ApplicantFields($applicant);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laranext\Recruitment\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Applicant $applicant)
    {
        return new ApplicantRequest($request, $applicant);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Applicant::destroy($request->items);

        return [
            'message' => count($request->items) > 1
                ? 'Applicants Deleted Successfully!'
                : 'Applicant Deleted Successfully!'
        ];
    }
}
