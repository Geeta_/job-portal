<?php

namespace Laranext\Recruitment\Http\Controllers;

use Laranext\Recruitment\Job;
use App\Http\Controllers\Controller;

class JobsController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource('job');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        return view('jobs.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Laranext\Recruitment\Job  $job
     * @return View
     */
    public function create(Job $job)
    {
        return view('jobs.form', compact('job'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \Laranext\Recruitment\Job  $job
     * @return View
     */
    public function show(Job $job)
    {
        return view('jobs.detail', compact('job'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Laranext\Recruitment\Job  $job
     * @return View
     */
    public function edit(Job $job)
    {
        return view('jobs.form', compact('job'));
    }
}
