<?php

namespace Laranext\Recruitment\Http\Controllers;

use Laranext\Recruitment\Category;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource('category');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Laranext\Recruitment\Category  $category
     * @return View
     */
    public function index(Category $category)
    {
        return view('categories.index', compact('category'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \Laranext\Recruitment\Category  $category
     * @return View
     */
    public function show(Category $category)
    {
        return view('categories.detail', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Laranext\Recruitment\Category  $category
     * @return View
     */
    public function edit(Category $category)
    {
        return view('categories.index', compact('category'));
    }
}
