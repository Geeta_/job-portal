<?php

namespace Laranext\Recruitment\Http\Controllers;

use Laranext\Recruitment\Metrics;
use App\Http\Controllers\Controller;

class ShowDashboard extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Metrics $metrics)
    {
        return view('dashboard', compact('metrics'));
    }
}
