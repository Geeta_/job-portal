<?php

namespace Laranext\Recruitment\Http\Controllers;

use Laranext\Recruitment\Process;
use App\Http\Controllers\Controller;

class ProcessesController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource('process');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Laranext\Recruitment\Process  $process
     * @return View
     */
    public function index(Process $process)
    {
        return view('processes.index', compact('process'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \Laranext\Recruitment\Process  $process
     * @return View
     */
    public function show(Process $process)
    {
        return view('processes.detail', compact('process'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Laranext\Recruitment\Process  $process
     * @return View
     */
    public function edit(Process $process)
    {
        return view('processes.index', compact('process'));
    }
}
