<?php

namespace Laranext\Recruitment\Http\Controllers;

use Laranext\Recruitment\Type;
use App\Http\Controllers\Controller;

class TypesController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource('type');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Laranext\Recruitment\Type  $type
     * @return View
     */
    public function index(Type $type)
    {
        return view('types.index', compact('type'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \Laranext\Recruitment\Type  $type
     * @return View
     */
    public function show(Type $type)
    {
        return view('types.detail', compact('type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Laranext\Recruitment\Type  $type
     * @return View
     */
    public function edit(Type $type)
    {
        return view('types.index', compact('type'));
    }
}
