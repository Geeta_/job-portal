<?php

namespace Laranext\Recruitment\Http\Controllers;

use Illuminate\Http\Request;
use Laranext\Recruitment\Process;
use Laranext\Recruitment\Applicant;
use App\Http\Controllers\Controller;

class ApplicantsController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource('applicant');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        return request()->has('list')
                ? view('applicants.index')
                : view('applicants.kanban');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Laranext\Recruitment\Applicant  $applicant
     * @return View
     */
    public function create(Applicant $applicant)
    {
        return view('applicants.form', compact('applicant'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \Laranext\Recruitment\Applicant  $applicant
     * @return View
     */
    public function show(Applicant $applicant)
    {
        $processes = Process::get(['id', 'name']);

        return view('applicants.detail', compact('applicant', 'processes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Laranext\Recruitment\Applicant  $applicant
     * @return View
     */
    public function edit(Applicant $applicant)
    {
        return view('applicants.form', compact('applicant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laranext\Recruitment\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Applicant $applicant)
    {
        $applicant->update([
            'process_id' => $request->process
        ]);

        return back();
    }
}
