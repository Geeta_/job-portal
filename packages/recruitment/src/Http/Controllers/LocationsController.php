<?php

namespace Laranext\Recruitment\Http\Controllers;

use Laranext\Recruitment\Location;
use App\Http\Controllers\Controller;

class LocationsController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource('location');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Laranext\Recruitment\Location  $location
     * @return View
     */
    public function index(Location $location)
    {
        return view('locations.index', compact('location'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \Laranext\Recruitment\Location  $location
     * @return View
     */
    public function show(Location $location)
    {
        return view('locations.detail', compact('location'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Laranext\Recruitment\Location  $location
     * @return View
     */
    public function edit(Location $location)
    {
        return view('locations.index', compact('location'));
    }
}
