<?php

namespace Laranext\Recruitment\Fields;

use Laranext\Fields\Date;
use Laranext\Fields\Text;
use Laranext\Fields\Radio;
use Laranext\Fields\Fields;
use Laranext\Fields\Select;
use Laranext\Fields\Textarea;
use Laranext\Recruitment\Type;
use Laranext\Recruitment\Category;
use Laranext\Recruitment\Location;

class JobFields extends Fields
{
    /**
     * Get the fields displayed by the resource.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Text::make(trans_choice('messages.words.title', 1), 'title')
                ->value($this->model->title)
                ->inline()
                ->required(),

            Select::make(trans_choice('messages.words.category', 1), 'category')
                ->value($this->model->cat_id)
                ->options(Category::get(['id', 'name']))
                ->inline()
                ->required(),

            Select::make(trans_choice('messages.words.location', 1), 'location')
                ->value($this->model->location_id)
                ->options(Location::get(['id', 'name']))
                ->inline()
                ->required(),

            Radio::make(trans_choice('messages.words.type', 1), 'type')
                ->value($this->model->type_id)
                ->options(Type::get(['id', 'name']))
                ->inline()
                ->required(),

            Text::make(trans_choice('messages.words.salary', 1), 'salary')
                ->value($this->model->salary)
                ->inline()
                ->required(),

            Date::make(trans_choice('messages.miscellaneous.expire_at', 1), 'expired_at')
                ->value(optional($this->model->expired_at)->format('Y-m-d'))
                ->inline()
                ->required(),

            Textarea::make(trans_choice('messages.words.description', 1), 'description')
                ->value($this->model->description)
                ->info('* You may use Markdown with <a href="https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet" class="text-blue-500">GitHub-flavored</a> code blocks.')
                ->inline()
                ->required(),
        ];
    }

    /**
     * Get the index fields displayed by the resource.
     *
     * @return array
     */
    public function indexFields()
    {
        return [
            Text::make(trans_choice('messages.words.title', 1), 'title')
                ->value($this->model->title)
                ->sortable(),

            Text::make(trans_choice('messages.words.category', 1), 'category')
                ->value($this->model->category->name),

            Text::make(trans_choice('messages.words.location', 1), 'location')
                ->value($this->model->location->name),

            Text::make(trans_choice('messages.words.applicant', 2), 'applicants')
                ->value($this->model->applicants->count())
                ->align('center')
        ];
    }
}
