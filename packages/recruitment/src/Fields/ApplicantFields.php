<?php

namespace Laranext\Recruitment\Fields;

use Laranext\Fields\Link;
use Laranext\Fields\Text;
use Laranext\Fields\Fields;

class ApplicantFields extends Fields
{
    /**
     * Get the fields displayed by the resource.
     *
     * @return array
     */
    public function fields()
    {
        return [];
    }

    /**
     * Get the index fields displayed by the resource.
     *
     * @return array
     */
    public function indexFields()
    {
        return [
            Text::make(trans_choice('messages.words.name', 1), 'name')
                ->value($this->model->name)
                ->sortable(),

            Link::make(trans_choice('messages.words.job', 1), 'job')
                ->value($this->model->job->title)
                ->url('/' . config('laranext.admin_prefix') . '/recruitment/jobs/'.$this->model->job->id),

            Text::make(trans_choice('messages.words.status', 1), 'status')
                ->value($this->model->process->name),
        ];
    }
}
