<?php

namespace Laranext\Recruitment\Actions;

use Laranext\Actions\Actions;

class JobActions extends Actions
{
    /**
     * Get the actions available for the resource.
     *
     * @return array
     */
    public function actions()
    {
        return [];
    }
}
