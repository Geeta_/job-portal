<?php

namespace Laranext\Recruitment\Actions;

use Laranext\Fields\Select;
use Laranext\Actions\Action;
use Laranext\Recruitment\Process;
use Laranext\Recruitment\Applicant;
use Illuminate\Support\Facades\Validator;

class UpdateProcess extends Action
{
    /**
     * The displayable danger of the action.
     *
     * @var string
     */
    public $danger = false;

    /**
     * Get the displayable name of the action.
     *
     * @return string
     */
    public function name()
    {
        return __('messages.miscellaneous.update_process');
    }

    /**
     * Get the displayable button of the action.
     *
     * @return string
     */
    public function button()
    {
        return trans_choice('messages.words.run', 1);
    }

    /**
     * Perform the action on the given models.
     *
     * @param  $items
     * @param  $fields
     * @return mixed
     */
    public function handle($items, $fields)
    {
        Validator::make($fields, [
            'process' => 'required',
        ])->validate();

        foreach ($items as $item) {
            $applicant = Applicant::find($item);

            $applicant->update([
                'process_id' => $fields['process']
            ]);
        }

        return Action::message('Process updated successfully!');
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Select::make(trans_choice('messages.words.process', 1), 'process')
                ->options(Process::get(['id', 'name']))
                ->required(),
        ];
    }
}
