<?php

namespace Laranext\Recruitment\Actions;

use Laranext\Actions\Actions;

class ApplicantActions extends Actions
{
    /**
     * Get the actions available for the resource.
     *
     * @return array
     */
    public function actions()
    {
        return [
            new UpdateProcess
        ];
    }
}
