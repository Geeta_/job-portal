<?php

namespace Laranext\Recruitment\Actions;

use Laranext\Actions\Actions;

class CategoryActions extends Actions
{
    /**
     * Get the actions available for the resource.
     *
     * @return array
     */
    public function actions()
    {
        return [];
    }
}
