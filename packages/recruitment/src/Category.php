<?php

namespace Laranext\Recruitment;

use Laranext\Categorizable;
use Illuminate\Database\Eloquent\Model;
use Laranext\Recruitment\Filters\CategoryFilters;

class Category extends Model
{
    use Categorizable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The Category Parent.
     *
     * @var string
     */
    protected static $parent = 'Job Category';

    /**
     * The jobs that belong to the category.
     */
    public function jobs()
    {
        return $this->hasMany(Job::class, 'cat_id');
    }

    /**
     * Apply all relevant category filters.
     *
     * @param  Builder       $query
     * @param  Laranext\Recruitment\CategoryFilters $filters
     * @return Builder
     */
    public function scopeFilter($query, CategoryFilters $filters)
    {
        return $filters->apply($query);
    }
}
