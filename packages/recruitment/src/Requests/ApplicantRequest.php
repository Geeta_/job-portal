<?php

namespace Laranext\Recruitment\Requests;

use Laranext\Requests\FormRequest;

class ApplicantRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
