<?php

namespace Laranext\Recruitment\Requests;

use Laranext\Requests\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => 'required|unique:categories,name,' . $this->model->id
        ];
    }

    /**
     * Get custom attributes.
     *
     * @return array
     */
    public function customAttributes()
    {
        return [
            'category' => 'name'
        ];
    }
}
