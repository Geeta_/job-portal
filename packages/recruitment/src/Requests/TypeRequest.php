<?php

namespace Laranext\Recruitment\Requests;

use Laranext\Requests\FormRequest;

class TypeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|unique:categories,name,' . $this->model->id
        ];
    }

    /**
     * Get custom attributes.
     *
     * @return array
     */
    public function customAttributes()
    {
        return [
            'type' => 'name'
        ];
    }
}
