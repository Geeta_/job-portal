<?php

namespace Laranext\Recruitment\Requests;

use Laranext\Requests\FormRequest;

class ProcessRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'process' => 'required|unique:categories,name,' . $this->model->id
        ];
    }

    /**
     * Get custom attributes.
     *
     * @return array
     */
    public function customAttributes()
    {
        return [
            'process' => 'name'
        ];
    }
}
