<?php

namespace Laranext\Recruitment\Requests;

use Illuminate\Support\Facades\DB;
use Laranext\Requests\FormRequest;

class JobRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string', 'max:255', 'unique:jobs,title,' . $this->model->id],
            'category' => ['required', 'integer'],
            'location' => ['required', 'integer'],
            'type' => ['required', 'integer'],
            'salary' => ['required', 'integer'],
            'description' => ['required', 'string'],
            'expired_at' => ['required'],
        ];
    }

    /**
     * Database Transaction.
     *
     * @return void
     */
    public function transaction()
    {
        $this->attributes['cat_id'] = $this->attributes['category'];
        $this->attributes['type_id'] = $this->attributes['type'];
        $this->attributes['location_id'] = $this->attributes['location'];
        unset($this->attributes['type']);
        unset($this->attributes['category']);
        unset($this->attributes['location']);

        DB::transaction(function () {
            $this->model->forceFill($this->attributes);

            $this->model->save();
        });
    }
}
