<?php

namespace Laranext\Recruitment;

use Laranext\Categorizable;
use Illuminate\Database\Eloquent\Model;
use Laranext\Recruitment\Filters\ProcessFilters;

class Process extends Model
{
    use Categorizable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The Category Parent.
     *
     * @var string
     */
    protected static $parent = 'Hiring Process';

    /**
     * The applicants that belong to the process.
     */
    public function applicants()
    {
        return $this->hasMany(Applicant::class);
    }

    /**
     * Apply all relevant process filters.
     *
     * @param  Builder       $query
     * @param  Laranext\Recruitment\ProcessFilters $filters
     * @return Builder
     */
    public function scopeFilter($query, ProcessFilters $filters)
    {
        return $filters->apply($query);
    }
}
