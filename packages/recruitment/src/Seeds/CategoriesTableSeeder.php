<?php

namespace Laranext\Recruitment\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Job Category',
                'key' => 'Job Category'
            ],
            [
                'name' => 'Job Type',
                'key' => 'Job Type'
            ],
            [
                'name' => 'Job Location',
                'key' => 'Job Location'
            ],
            [
                'name' => 'Hiring Process',
                'key' => 'Hiring Process'
            ],
        ];

        DB::table('categories')->insert($categories);
    }
}
