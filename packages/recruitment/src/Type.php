<?php

namespace Laranext\Recruitment;

use Laranext\Categorizable;
use Illuminate\Database\Eloquent\Model;
use Laranext\Recruitment\Filters\TypeFilters;

class Type extends Model
{
    use Categorizable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The Category Parent.
     *
     * @var string
     */
    protected static $parent = 'Job Type';

    /**
     * The jobs that belong to the type.
     */
    public function jobs()
    {
        return $this->hasMany(Job::class);
    }

    /**
     * Apply all relevant type filters.
     *
     * @param  Builder       $query
     * @param  Laranext\Recruitment\TypeFilters $filters
     * @return Builder
     */
    public function scopeFilter($query, TypeFilters $filters)
    {
        return $filters->apply($query);
    }
}
