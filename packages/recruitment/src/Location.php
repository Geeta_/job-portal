<?php

namespace Laranext\Recruitment;

use Laranext\Categorizable;
use Illuminate\Database\Eloquent\Model;
use Laranext\Recruitment\Filters\LocationFilters;

class Location extends Model
{
    use Categorizable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The Category Parent.
     *
     * @var string
     */
    protected static $parent = 'Job Location';

    /**
     * The jobs that belong to the location.
     */
    public function jobs()
    {
        return $this->hasMany(Job::class);
    }

    /**
     * Apply all relevant location filters.
     *
     * @param  Builder       $query
     * @param  Laranext\Recruitment\LocationFilters $filters
     * @return Builder
     */
    public function scopeFilter($query, LocationFilters $filters)
    {
        return $filters->apply($query);
    }
}
