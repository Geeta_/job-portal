<?php

namespace Laranext\Recruitment;

use Illuminate\Database\Eloquent\Model;
use Laranext\Recruitment\Filters\ApplicantFilters;

class Applicant extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'birthday',
    ];

    /**
     * Get the job that owns the applicant.
     */
    public function job()
    {
        return $this->belongsTo(Job::class);
    }

    /**
     * The process that belong to the applicant.
     */
    public function process()
    {
        return $this->belongsTo(Process::class);
    }

    /**
     * Apply all relevant applicant filters.
     *
     * @param  Builder       $query
     * @param  Laranext\Recruitment\ApplicantFilters $filters
     * @return Builder
     */
    public function scopeFilter($query, ApplicantFilters $filters)
    {
        return $filters->apply($query);
    }
}
