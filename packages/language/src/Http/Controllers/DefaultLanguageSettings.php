<?php

namespace Laranext\Language\Http\Controllers;

use App\Http\Controllers\Controller;

class DefaultLanguageSettings extends Controller
{
    public function __construct()
    {
        $this->middleware('can:setting');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function __invoke()
    {
        return view('default');
    }
}
