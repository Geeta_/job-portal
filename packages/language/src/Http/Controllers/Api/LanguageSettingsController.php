<?php

namespace Laranext\Language\Http\Controllers\Api;

use Laranext\Option;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laranext\Language\Fields\LanguageFields;
use Laranext\Language\Requests\LanguageRequest;

class LanguageSettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:setting');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Laranext\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function create($language, Option $option)
    {
        $option->language = $language;

        return new LanguageFields($option);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laranext\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function store($language, Request $request, Option $option)
    {
        $option->language = $language;

        return new LanguageRequest($request, $option);
    }
}
