<?php

namespace Laranext\Language\Http\Controllers;

use App\Http\Controllers\Controller;

class LanguageSettings extends Controller
{
    public function __construct()
    {
        $this->middleware('can:setting');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function __invoke($language)
    {
        return view('language', compact('language'));
    }
}
