<?php

namespace Laranext\Language\Requests;

use Laranext\UpdateEnvConfig;
use Illuminate\Support\Facades\DB;
use Laranext\Requests\FormRequest;

class DefaultRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'app_locale' => 'required'
        ];
    }

    /**
     * Get the laravel app configs to change.
     *
     * @return array
     */
    public function configs()
    {
        return [
            'APP_LOCALE' => 'app_locale',
        ];
    }

    /**
     * Database Transaction.
     *
     * @return void
     */
    public function transaction()
    {
        DB::transaction(function () {
            new UpdateEnvConfig($this->request, $this->configs());

            option(
                $this->request->all()
            );
        });
    }

    /**
     * Get the success response.
     *
     * @return array
     */
    public function response()
    {
        return [
            'message' => 'Settings Saved Successfully!'
        ];
    }
}
