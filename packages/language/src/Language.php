<?php

namespace Laranext\Language;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    /**
     * Scope a query to only include active languages.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', true);
    }
}
