<?php

namespace Laranext\Language\Fields;

use Laranext\Fields\Div;
use Laranext\Fields\Text;
use Illuminate\Support\Str;
use Laranext\Fields\Fields;

class LanguageFields extends Fields
{
    /**
     * Get the fields displayed by the resource.
     *
     * @return array
     */
    public function fields()
    {
        $words = [
            'Applicant',
            'Cancel',
            'Category',
            'Create',
            'Dashboard',
            'Delete',
            'Description',
            'Edit',
            'Email',
            'General',
            'Hired',
            'Job',
            'Kanban',
            'Language',
            'Location',
            'Logout',
            'Name',
            'Other',
            'Password',
            'Permission',
            'Phone',
            'Process',
            'Rejected',
            'Resource',
            'Resume',
            'Role',
            'Run',
            'Salary',
            'Save',
            'Search',
            'Setting',
            'Status',
            'Title',
            'Type',
            'Update',
            'User',
        ];

        $miscellaneous = [
            'Add Another',
            'Continue Editing',
            'Cover Letter',
            'Delete Confirmation Message',
            'Delete Resource',
            'Expire At',
            'Hiring Process',
            'List View',
            'New Applicant',
            'No Record',
            'Update Process',
            'Visit Site',
        ];

        return [
            'Single Words' => collect($words)->map(function($value) {
                                  return Text::make($value, $this->model->language . '.words.' . Str::slug(strtolower($value), '_'))
                                            ->value(option($this->model->language . '.words.' . Str::slug(strtolower($value), '_')))
                                            ->inline();
                              }),

            'Pagination' => [
                    Text::make('Previous', $this->model->language . '.pagination.previous')
                        ->value(option($this->model->language . '.pagination.previous'))
                        ->inline(),

                    Text::make('Next', $this->model->language . '.pagination.next')
                        ->value(option($this->model->language . '.pagination.next'))
                        ->inline(),
            ],

            'Miscellaneous' => collect($miscellaneous)->map(function($value) {
                                   return Text::make($value, $this->model->language . '.miscellaneous.' . Str::slug(strtolower($value), '_'))
                                                ->value(option($this->model->language . '.miscellaneous.' . Str::slug(strtolower($value), '_')))
                                                ->inline();
                               }),
        ];
    }
}
