<?php

namespace Laranext\Language\Fields;

use Laranext\Fields\Fields;
use Laranext\Fields\Select;
use Laranext\Language\Language;

class DefaultFields extends Fields
{
    /**
     * Get the fields displayed by the resource.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Select::make('Language', 'app_locale')
                ->inline()
                ->value(option('app_locale'))
                ->options(Language::active()->get()->map(function ($value, $key) {
                    return ['id' => $value->code, 'name' => $value->name];
                })),
        ];
    }
}
