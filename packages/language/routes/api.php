<?php

use Illuminate\Support\Facades\Route;

Route::get('{resource}/actions', 'ActionsController@index');
Route::post('{resource}/action', 'ActionsController@store');
Route::get('{resource}/filters', 'FiltersController@index');

Route::resource('default', DefaultLanguageSettingsController::class)->only(['create', 'store']);
Route::resource('{language}', LanguageSettingsController::class)->only(['create', 'store']);
