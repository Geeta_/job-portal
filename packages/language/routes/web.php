<?php

use Illuminate\Support\Facades\Route;

Route::get('default', DefaultLanguageSettings::class);
Route::get('{language}', LanguageSettings::class);
