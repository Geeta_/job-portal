@extends('master')

@section('topbar')
    <div class="flex items-center text-sm text-gray-700 uppercase font-bold tracking-widest py-6">
        {{ trans_choice('messages.words.language', 2) }}
    </div>
@endsection

@section('content')
    <v-form name="setting"
            route="default"
            save-only
    ></v-form>
@endsection
