@extends('layouts.split-menu')

@section('menu')
    <div class="card overflow-hidden py-2 w-56">
        <a href="/{{ config('laranext.admin_prefix') }}/languages/default" class="sub-menu-item {{ isActive('default', 3) }}">Default Language</a>
        <a href="/{{ config('laranext.admin_prefix') }}/languages/en" class="sub-menu-item {{ isActive('en', 3) }}">English</a>
        <a href="/{{ config('laranext.admin_prefix') }}/languages/fr" class="sub-menu-item {{ isActive('fr', 3) }}">French</a>
    </div>
@endsection
