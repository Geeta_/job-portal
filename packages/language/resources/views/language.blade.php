@extends('master')

@section('topbar')
    <div class="flex items-center text-sm text-gray-700 uppercase font-bold tracking-widest py-6">
        {{ trans_choice('messages.words.language', 2) }}
    </div>
@endsection

@section('content')
    <div class="card mb-4">
        <p class="text-sm text-gray-700 py-4 px-5">
            <span class="text-red-500">Note:</span> For pluralization please use "|" like this "There is one apple | There are many apples"
        </p>
    </div>

    <v-form namespace="setting"
            route="{{ $language }}"
            tabs
            save-only
    ></v-form>
@endsection
