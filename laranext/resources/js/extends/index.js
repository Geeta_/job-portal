import Vue from 'vue'

let components = [
    'Kanban',
    'Notes'
]

for (let i = 0; i < components.length; i++) {
    const component = components[i]

    Vue.component(
        component,
        require('./components/' + component).default
    )
}
