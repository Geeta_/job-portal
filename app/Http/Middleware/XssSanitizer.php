<?php

namespace App\Http\Middleware;

use Stevebauman\Purify\Facades\Purify;
use Illuminate\Foundation\Http\Middleware\TransformsRequest;

class XssSanitizer extends TransformsRequest
{
    /**
     * Transform the given value.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    protected function transform($key, $value)
    {
        return Purify::clean($value);
    }
}
